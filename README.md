# Gipfeli - Python implementation

Gipfeli is a high-speed compression algorithm based on LZ77 and entropy coding. This repository provides an implementation of the algorithm in pure Python.

The code is based on the reference implementation by Rastislav Lenhardt (in C++), version [`04fe241`](https://github.com/google/gipfeli/tree/04fe241e27f6dcfef239afc6c5e3cee0b4d7c333). It has been ported to Python while trying to make it more verbose and improving the documentation - for this reason the code and the comments tend to be very similar to the original implementation. The stream support has been omitted.

The original code is available [on GitHub](https://github.com/google/gipfeli) under the [3-Clause BSD License](https://github.com/google/gipfeli/blob/master/COPYING). The corresponding paper can be found [here](https://static.googleusercontent.com/media/research.google.com/de//pubs/archive/42050.pdf).

For convenience a terminal interface similar to the one used by the Python version is provided for the C++ version inside the `tools` directory. Please make sure that you have cloned this repository including the submodules if you want to use it. You may use the provided Makefile afterwards. Be aware of the fact that this interface implementation reads the complete file into memory before compressing it inside memory and writing it back to the hard drive at the end of the process - this may lead to problems with bigger files.

This project has been developed to get some insights on how the Gipfeli algorithm works in detail as a part of an university project. The implementation is rather slow and probably not suited for production, but may be used for educational purposes. By default no hashing function is used for the LZ77 dictionary lookup.

I will not guarantee that the code inside this repository works correct in all cases. Especially the unaligned load and store operations have only been tested on little-endian devices. Use it at your own risk!

## Running

Make sure you have 

* Python 3

installed on your device.

Afterwards you can run `python3 -m gipfeli` from the terminal.

To make use of the terminal interface, you should have a look at the parameters using `python3 -m gipfeli --help`.

## Development tasks

### Tests

After installing

* [lipsum](https://github.com/thanethomson/lipsum)

you should be able to run `python3 -m unittest discover --verbose --start-directory tests` from the root directory of this repository.

### Documentation

You need the following Python modules:

* [Sphinx](http://www.sphinx-doc.org/en/stable/)
* [sphinx_rtd_theme](https://github.com/rtfd/sphinx_rtd_theme/)
* [GitPython](https://github.com/gitpython-developers/GitPython)

Then you should be able to run `make clean && make html` from the `docs` directory to get the documentation in HTML format. Use `make latex` to create LaTeX source files.

## Citing

If you want to cite this code in your own paper, feel free to write me a message. I will provide you with the corresponding entry in this case.

## License

The provided code is distributed under the terms of the [3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause).

Please note that some of the test files are published under a different license. See the license file in the corresponding directory for details.
