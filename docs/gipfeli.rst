gipfeli package
===============

This page contains the inline documentation, generated from the code using Sphinx.

Modules
-------

`gipfeli.interface` -- Basic Interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.interface


`gipfeli.common.bits` -- Bit-based Handlers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.common.bits


`gipfeli.common.utils` -- Common Utility Methods
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.common.utils


`gipfeli.compression.compression` -- Compression Interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.compression.compression


`gipfeli.compression.compression` -- Entropy-coding Handlers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.compression.entropy


`gipfeli.compression.hashing` -- Hashing Handlers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.compression.hashing


`gipfeli.compression.lz77` -- LZ77-related Functionality
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.compression.lz77


`gipfeli.compression.matching` -- Matching Handlers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.compression.matching


`gipfeli.decompression.decompression` -- Decompression Interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: gipfeli.decompression.decompression
