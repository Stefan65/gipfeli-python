Welcome to the documentation of Gipfeli!
========================================

This is the public API documentation for the Python port of the Gipfeli data compression algorithm.

The code is based on the reference implementation by Rastislav Lenhardt (in C++). It has been ported to Python while trying to make it more verbose and improving the documentation.

The original code is available `on GitHub <https://github.com/google/gipfeli>`_ under the `3-Clause BSD License <https://github.com/google/gipfeli/blob/master/COPYING>`_. The corresponding paper can be found `here <https://static.googleusercontent.com/media/research.google.com/de//pubs/archive/42050.pdf>`_.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    gipfeli


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
