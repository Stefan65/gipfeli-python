#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The main module of the application. This is executed when calling
``python3 -m gipfeli`` from the parent directory.
"""

import argparse

from gipfeli.interface import Gipfeli

# Instantiate an argument parser.
PARSER = argparse.ArgumentParser(description="Gipfeli Terminal Interface")

# Add the switches.
GROUP = PARSER.add_mutually_exclusive_group()
GROUP.add_argument(
    "-c", "--compress", dest="compress", action="store_true", help="Compress a file."
)
GROUP.add_argument(
    "-d",
    "--decompress",
    "-u",
    "--uncompress",
    dest="uncompress",
    action="store_true",
    help="Decompress/uncompress a file.",
)

PARSER.add_argument(
    "-l",
    "--list",
    dest="as_list",
    action="store_true",
    help="Print the output as a list instead of a string.",
)

# Add the arguments.
PARSER.add_argument(
    "infile", type=str, default=None, nargs=1, help="The name of the input file."
)
PARSER.add_argument(
    "outfile",
    type=str,
    default=None,
    nargs="?",
    help="The name of the output file. If no file is "
    "specified, the output gets written to the terminal.",
)

# Start parsing the user input.
ARGUMENTS = PARSER.parse_args()

# We must have an input file. The parser already handles this, but just make
# this sure again.
if not ARGUMENTS.infile:
    raise ValueError("An input file has to be specified.")

# We get lists, but only need the first one.
if isinstance(ARGUMENTS.infile, list):
    ARGUMENTS.infile = ARGUMENTS.infile[0]
if ARGUMENTS.outfile and isinstance(ARGUMENTS.outfile, list):
    ARGUMENTS.outfile = ARGUMENTS.outfile[0]

# Delegate the work to the interface methods.
if ARGUMENTS.compress or not ARGUMENTS.uncompress:
    COMPRESSED = Gipfeli.compress_file(ARGUMENTS.infile, ARGUMENTS.outfile)
    if not ARGUMENTS.outfile:
        print(list(COMPRESSED) if ARGUMENTS.as_list else COMPRESSED)
elif ARGUMENTS.uncompress or not ARGUMENTS.compress:
    UNCOMPRESSED = Gipfeli.uncompress_file(ARGUMENTS.infile, ARGUMENTS.outfile)
    if not ARGUMENTS.outfile:
        print(list(UNCOMPRESSED) if ARGUMENTS.as_list else UNCOMPRESSED)
else:
    raise ValueError(
        "Got an unexpected value and could not decide which " "action to perform."
    )
