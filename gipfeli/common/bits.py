#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
A collection of bit-based handlers.
"""

from gipfeli.common.utils import UnalignedDataAccess


class BitWriter:
    """
    Methods to write bits to the output.

    They always have to be used in the following order:

    .. code-block:: python

       bitWriter = BitWriter()
       bitWriter.write(bit_length, value)
       bitWriter.write(bit_length, value)
       ...
       bitWriter.flush()
       output += bitWriter.output
    """

    _output = ""
    """
    The output data.

    :type: :class:`str`
    """

    _bits = 0
    """
    The current number of bits inside :attr:`_bit_buffer_64`.

    :type: :class:`int`
    """

    _bit_buffer_64 = 0
    """
    The current buffer value.

    :type: :class:`int`
    """

    def __init__(self):
        """
        Instead of a start method, we just use the :code:`__init__` method as
        the initial values will never be determined by an outer construct.
        """
        self._output = ""
        self._bits = 0
        self._bit_buffer_64 = 0

    def write(self, length, value):
        """
        Write the given value to the output using the given number of bits.

        Some sort of buffering is performed: Instead of writing the data
        directly to the output, we wait until the sum of all length values has
        reached 64. In this case, we add the buffer content to the output and
        restart with an empty buffer (or the remaining bits of the data if the
        sum of the lengths exceeds 64).

        :param length: The number of bits to use.
        :type length: int

        :param value: The value to write.
        :type value: int
        """
        v = 64 - length
        if self._bits < v:
            # No split needed. We just append the binary representation of the
            # value to the binary representation of the current buffer.
            # Afterwards the bit counter gets increased.
            self._bit_buffer_64 <<= length
            self._bit_buffer_64 |= value
            self._bits += length
        else:
            # Split needed. We append the binary representation of the first
            # part of value to the binary representation of the current buffer
            # so that the number of bits inside the buffer is 64. Afterwards
            # we set the bit counter to the number of bits which have not yet
            # been used from the value.
            self._bit_buffer_64 <<= 64 - self._bits
            self._bits -= v
            self._bit_buffer_64 |= value >> self._bits

            # Write the 64 bit value to the output.
            self._output += UnalignedDataAccess.unaligned_store_64(
                self._bit_buffer_64, "str"
            )

            # We can write the whole value into the bit buffer as consequent
            # method calls will automatically erase the extra high bits.
            self._bit_buffer_64 = value

    def flush(self):
        """
        Add the remaining bit buffer to the output.
        """
        if self._bits != 0:
            # As there might be old values in the buffer because they are not
            # getting erased on block borders, we have to shift the values of
            # the current block to the front. All bits following the set bits
            # of the current block will be zero because of this.
            self._bit_buffer_64 <<= 64 - self._bits
            self._output += UnalignedDataAccess.unaligned_store_64(
                self._bit_buffer_64, "str"
            )

    @property
    def output(self):
        """
        The part of the data which has been written by the bit writer.

        .. note::

           This property is read-only.

        .. note::

           Make sure to flush the writer before retrieving the output.
           Otherwise you might loose up to 63 bits of data.

        :type: :class:`str`
        """
        return self._output


class BitReader:
    """
    Methods to read input bit by bit.

    Sample usage:

    .. code-block:: python

       input_data = "My input data"
       input_end_pos = len(input_data)
       bits = BitReader(input_data, input_end_pos)
       x = bits.read(3)
       ...
       y = bits.read(7)
       remaining_data = bits.stop()
    """

    _bits_left = 64
    """
    The number of bits left for the current 64 bit integer value.

    :type: :class:`int`
    """

    _current = 0
    """
    The current 64 bit integer value.

    :type: :class:`int`
    """

    _input_pos = 0
    """
    The current index inside the input data.

    :type: :class:`int`
    """

    _end_pos = 0
    """
    The maximum index which is allowed inside the input data.

    :type: :class:`int`
    """

    _data = ""
    """
    The input data itself to read from.

    :type: :class:`str`
    """

    def __init__(self, data, index_start, end_pos):
        """
        When starting the reader, we need to pass the input data and the
        maximal index to use inside it.

        :param data: The data to read the bits from.
        :type data: str

        :param index_start: The index to start at.
        :type index_start: int

        :param end_pos: The maximal index to use inside the the input data.
        :type end_pos: int
        """
        self._current = UnalignedDataAccess.unaligned_load_64(
            data[index_start:], return_type="int"
        )
        self._current = bin(self._current)[2:].zfill(64)
        self._input_pos = index_start + 8
        self._end_pos = end_pos
        self._data = data

    def read(self, length):
        """
        Read the requested number of bits from the input data.

        :param length: The number of bits to read.
        :type length: int

        :return: The read value.
        :rtype: int

        :raises ValueError: The current position inside the input has exceeded
                            the maximum index which has been allowed before.
        """
        ret = ""  # 0
        if length <= self._bits_left:
            # There are enough bits left from the currently loaded value.
            # The return value is built from the first :code:`length` bits -
            # which are retrieved from the value by cutting away all the
            # unneeded bits on the right of the binary representation of the
            # value.
            ret = self._current[0:length]
            # ret = self._current >> (64 - length)
            # We have read the first :code:`length` bits, so we cut them off
            # from the binary representation.
            self._current = self._current[length:]
            # self._current <<= length
            # We read :code:`length` bits, so the number of remaining bits
            # needs to be reduced.
            self._bits_left -= length
        else:
            # There are not enough bits left from the currently loaded value.

            # Determine the first part of the return value.
            if self._bits_left > 0:
                # There are a few bits left from the old value, so add them at
                # the beginning of the return value. When adding the other bits
                # to the return value in the second part, they just get
                # appended.
                ret = self._current
                # ret = (self._current >> (64 - self._bits_left)) \
                #       << (length - self._bits_left)
            # We have read some bits from the old value and therefore can
            # reduce the remaining number of bits to read from the new value.
            length -= self._bits_left

            # Try to load a new value (64 bits = 8 bytes) from the input.
            if (self._input_pos + 8) > self._end_pos:
                # There are not enough bytes left to satisfy the request. For
                # this reason, we are throwing an exception here as the input
                # might be broken.
                self._bits_left = 0
                raise ValueError("Too much bits requested to be read.")
            # Load the new value from the input data.
            self._current = UnalignedDataAccess.unaligned_load_64(
                self._data[self._input_pos :], return_type="int"
            )
            self._current = bin(self._current)[2:].zfill(64)
            # We have read 8 bytes = 8 characters from the input data string,
            # so we have to increase our input position.
            self._input_pos += 8

            # Determine the second part of the return value.
            # Nearly the same as in the other branch - only the number of
            # remaining bits is determined different: We start with the total
            # of 64 bits again and reduce it by the remaining length.
            ret += self._current[0:length]
            self._current = self._current[length:]
            # ret += self._current >> (64 - length)
            # self._current <<= length
            self._bits_left = 64 - length

        ret = int(ret, 2)

        return ret

    def stop(self):
        """
        Stop the reader by returning the current index.

        :return: The current index.
        :rtype: int
        """
        return self._input_pos
