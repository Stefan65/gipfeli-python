#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
A collection of utilities needed by different parts of the code.
"""

import math


class Math:
    """
    A collection of helper methods regarding math operations.
    """

    @staticmethod
    def is_power(n, k):
        """
        Check if :data:`n` is a power of :data:`k`.

        :param n: The value to check.
        :type n: int

        :param k: The base to check the power with.
        :type k: int

        :return: :code:`True` if :data:`n` can be written as :code:`n = k^m`
                 (with :code:`m` being an integer value), :code:`False`
                 otherwise.
        :rtype: bool
        """
        # We always have positive numbers.
        if n < 0:
            return False

        if n == k:
            return True
        return n == k ** int(round(math.log(n, k)))

    @staticmethod
    def is_divisible(n, k):
        """
        Check if :data:`n` is divisible by :data:`k`.

        :param n: The value to check.
        :type n: int

        :param k: The value to divide by.
        :type k: int

        :return: :code:`True` if :data:`n` is divisible by :data:`k` without a
                 remainder, :code:`False` otherwise.
        :rtype: bool
        """
        return n % k == 0

    @staticmethod
    def log2_floor(n):
        """
        Calculate the binary logarithm of the given number and round the result
        to the floor.

        :param n: The value to perform the calculation on.
        :type n: int

        :return: The binary logarithm.
        :rtype: int
        """
        return math.floor(math.log2(n))

    @staticmethod
    def log2_floor_non_zero(n):
        """
        Calculate the binary logarithm of the given number and round the result
        to the floor. Zero is not allowed as a value.

        :param n: The value to perform the calculation on. Cannot be zero.
        :type n: int

        :return: The binary logarithm.
        :rtype: int

        :raises ValueError: The input value is zero.
        """
        if n == 0:
            raise ValueError("The input cannot be zero.")
        return Math.log2_floor(n)

    @staticmethod
    def count_ones(n):
        """
        Count the number of ones inside the binary representation of the given
        number.

        This uses the Hamming weight method, taken from
        https://stackoverflow.com/a/109025/.

        :param n: The number to count inside.
        :type n: int

        :return: The number of ones inside the value.
        :rtype: int
        """
        n -= (n >> 1) & 0x55555555
        n = (n & 0x33333333) + ((n >> 2) & 0x33333333)
        return (((n + (n >> 4) & 0xF0F0F0F) * 0x1010101) & 0xFFFFFFFF) >> 24
        # Alternative more verbose solution.
        # return bin(n).count("1", 2)


class UnalignedDataAccess:
    """
    Compatibility class to provide methods for retrieving a specified number of
    bytes from the input.

    These methods have been tested to be compatible with the corresponding
    methods from the :code:`stubs-internal.h`  of the reference implementation.

    .. note::

       The compatibility tests have only been done on little endian systems.
       For this reason the Python and original C++ version might not be
       compatible to each other on big endian systems.
    """

    @staticmethod
    def unaligned_load(
        input_data, number_of_bits=0, number_of_bytes=0, return_type="str"
    ):
        """
        Get the requested amount of bytes from the beginning of the given input
        data for loading it.

        The parameter for the number of bits and the number of bytes are
        mutually exclusive. Calling the method with both of them set to a
        "real" value will throw an exception.

        If a string is requested as a return value, the method just does some
        string slicing. If an integer gets requested, we are doing some binary
        operations which makes this method compatible to the ones in the
        reference implementation.

        :param input_data: The input to get the requested number of characters
                           from.
        :type input_data: str

        :param number_of_bits: The number of bits to extract. This needs to be
                               a positive number and divisible by 8 (because 8
                               bit = 1 byte = 1 character).
        :type number_of_bits: int

        :param number_of_bytes: The number of bytes aka the number of
                                characters to extract.
        :type number_of_bytes: int

        :param return_type: The needed return type. Supports :code:`"int"` and
                            :code:`"str"`.
        :type return_type: str

        :return: The requested number of characters from the input string.
        :rtype: int or str

        :raises ValueError: Both a number of bits and a number of bytes have
                            been specified, the number of bits does not meet
                            the requirements or the requested return type is
                            not supported.
        """
        # They are mutually exclusive.
        if number_of_bits and number_of_bytes:
            raise ValueError(
                "You cannot specify a number of bits and a number"
                " of bytes at the same time as I do not know "
                "which one too choose."
            )

        # We operate on bytes as one byte means one character, so let us divide
        # the number of bits by 8 to get the corresponding number of bytes.
        if number_of_bits:
            if (number_of_bits <= 0) or not Math.is_divisible(number_of_bits, 8):
                raise ValueError("The number of bits has to be a power of 2.")
            number_of_bytes = number_of_bits >> 3

        # Handle valid return types.
        if return_type == "str":
            # If we need a string, we just need to apply some slicing.
            return input_data[0:number_of_bytes]
        if return_type == "int":
            # If we need an integer, we have to do some binary operations on
            # the ASCII values of the characters.
            value = 0
            for i in range(number_of_bytes):
                # Iterate over the substring in reverse order.
                data_value_index = number_of_bytes - i - 1
                if data_value_index < len(input_data):
                    data_value = ord(input_data[data_value_index])
                else:
                    data_value = 0
                value = (value << 8) | data_value
            return value

        # Handle invalid return types.
        raise ValueError(
            "The specified return type '{}' is unknown.".format(return_type)
        )

    @staticmethod
    def unaligned_store(
        input_data, number_of_bits=0, number_of_bytes=0, return_type="list"
    ):
        """
        Get the requested amount of bytes from the beginning of the given input
        data for storing it.

        The parameter for the number of bits and the number of bytes are
        mutually exclusive. Calling the method with both of them set to a
        "real" value will throw an exception.

        If a string is given as input, we just do some slicing. If we get an
        integer value (probably retrieved by using
        :func:`~UnalignedDataAccess.unaligned_load`), we decode it again to get
        the original characters. (The variant with the integer input is similar
        to the method in the reference implementation.)

        :param input_data: The input to get the requested number of characters
                           from.
        :type input_data: str or int

        :param number_of_bits: The number of bits to extract. This needs to be
                               a positive number and divisible by 8 (because 8
                               bit = 1 byte = 1 character).
        :type number_of_bits: int

        :param number_of_bytes: The number of bytes aka the number of
                                characters to extract.
        :type number_of_bytes: int

        :param return_type: The needed return type. Supports :code:`"list"`,
                            :code:`"str"` and :code:`"bytearray"`.
        :type return_type: str

        :return: The requested number of characters from the input.
        :rtype: list[int] or str or bytearray

        :raises ValueError: Both a number of bits and a number of bytes have
                            been specified, the number of bits does not meet
                            the requirements or the requested return type is
                            not supported.
        """
        # They are mutually exclusive.
        if number_of_bits and number_of_bytes:
            raise ValueError(
                "You cannot specify a number of bits and a number"
                " of bytes at the same time as I do not know "
                "which one too choose."
            )

        # We operate on bytes as one byte means one character, so let us divide
        # the number of bits by 8 to get the corresponding number of bytes.
        if number_of_bits:
            if (number_of_bits <= 0) or not Math.is_divisible(number_of_bits, 8):
                raise ValueError("The number of bits has to be a power of 2.")
            number_of_bytes = number_of_bits >> 3

        # Handle valid return types.
        if return_type in ["bytearray", "list", "str"]:
            # If we have an integer as input, we have to extract the original
            # characters from it. For a string we can just use slicing.
            if isinstance(input_data, int):
                values = []
                for i in range(number_of_bytes):
                    # Get the last 8 bits from the input (F_8 = 1111_2).
                    values.append(input_data & 0xFF)
                    # Remove the last 8 bits from the input by dividing the
                    # number by 2^8.
                    input_data >>= 8
            else:
                values = list(input_data[0:number_of_bytes])

            # Perform the conversion if needed.
            if return_type == "bytearray":
                return bytearray(values)
            if return_type == "list":
                return values
            if return_type == "str":
                for index, value in enumerate(values):
                    values[index] = chr(value) if isinstance(value, int) else value
                return "".join(values)

        # Handle invalid return types.
        raise ValueError(
            "The specified return type '{}' is unknown.".format(return_type)
        )

    @staticmethod
    def unaligned_load_64(input_data, return_type="str"):
        """
        Get the first 6 bytes from the beginning of the given input data for
        loading it.

        This is a compatibility method which calls
        :func:`~UnalignedDataAccess.unaligned_load`.

        :param input_data: The input to get the characters from.
        :type input_data: str

        :param return_type: The needed return type.
        :type return_type: str

        :return: The first 6 characters from the input string.
        :rtype: int or str
        """
        return UnalignedDataAccess.unaligned_load(
            input_data=input_data, number_of_bytes=8, return_type=return_type
        )

    @staticmethod
    def unaligned_load_32(input_data, return_type="str"):
        """
        Get the first 5 bytes from the beginning of the given input data for
        loading it.

        This is a compatibility method which calls
        :func:`~UnalignedDataAccess.unaligned_load`.

        :param input_data: The input to get the characters from.
        :type input_data: str

        :param return_type: The needed return type.
        :type return_type: str

        :return: The first 5 characters from the input string.
        :rtype: int or str

        :raises ValueError: Both a number of bits and a number of bytes have
                            been specified, the number of bits does not meet
                            the requirements or the requested return type is
                            not supported.
        """
        return UnalignedDataAccess.unaligned_load(
            input_data=input_data, number_of_bytes=4, return_type=return_type
        )

    @staticmethod
    def unaligned_load_16(input_data, return_type="str"):
        """
        Get the first 4 bytes from the beginning of the given input data for
        loading it.

        This is a compatibility method which calls
        :func:`~UnalignedDataAccess.unaligned_load`.

        :param input_data: The input to get the characters from.
        :type input_data: str

        :param return_type: The needed return type.
        :type return_type: str

        :return: The first 4 characters from the input string.
        :rtype: int or str

        :raises ValueError: Both a number of bits and a number of bytes have
                            been specified, the number of bits does not meet
                            the requirements or the requested return type is
                            not supported.
        """
        return UnalignedDataAccess.unaligned_load(
            input_data=input_data, number_of_bytes=2, return_type=return_type
        )

    @staticmethod
    def unaligned_store_64(input_data, return_type="str"):
        """
        Get the first 6 bytes from the beginning of the given input data for
        storing it.

        This is a compatibility method which calls
        :func:`~UnalignedDataAccess.unaligned_store`.

        :param input_data: The input to get the characters from.
        :type input_data: str or int

        :param return_type: The needed return type.
        :type return_type: str

        :return: The first 6 characters from the input.
        :rtype: list[int] or list[str] or bytearray

        :raises ValueError: Both a number of bits and a number of bytes have
                            been specified, the number of bits does not meet
                            the requirements or the requested return type is
                            not supported.
        """
        return UnalignedDataAccess.unaligned_store(
            input_data=input_data, number_of_bytes=8, return_type=return_type
        )

    @staticmethod
    def unaligned_store_32(input_data, return_type="str"):
        """
        Get the first 5 bytes from the beginning of the given input data for
        storing it.

        This is a compatibility method which calls
        :func:`~UnalignedDataAccess.unaligned_store`.

        :param input_data: The input to get the characters from.
        :type input_data: str or int

        :param return_type: The needed return type.
        :type return_type: str

        :return: The first 5 characters from the input.
        :rtype: list[int] or list[str] or bytearray

        :raises ValueError: Both a number of bits and a number of bytes have
                            been specified, the number of bits does not meet
                            the requirements or the requested return type is
                            not supported.
        """
        return UnalignedDataAccess.unaligned_store(
            input_data=input_data, number_of_bytes=4, return_type=return_type
        )

    @staticmethod
    def unaligned_store_16(input_data, return_type="str"):
        """
        Get the first 4 bytes from the beginning of the given input data for
        storing it.

        This is a compatibility method which calls
        :func:`~UnalignedDataAccess.unaligned_store`.

        :param input_data: The input to get the characters from.
        :type input_data: str or int

        :param return_type: The needed return type.
        :type return_type: str

        :return: The first 4 characters from the input.
        :rtype: list[int] or list[str] or bytearray

        :raises ValueError: Both a number of bits and a number of bytes have
                            been specified, the number of bits does not meet
                            the requirements or the requested return type is
                            not supported.
        """
        return UnalignedDataAccess.unaligned_store(
            input_data=input_data, number_of_bytes=2, return_type=return_type
        )


class IncrementalCopy:
    """
    Methods to perform incremental copies. This is needed if the input and
    output regions might overlap.
    """

    MAX_INCREMENT_COPY_OVERFLOW = 10
    """
    Use this value to prevent the worst case scenario in
    :func:`copy_fast_path`.

    :type: :class:`int`
    """

    @staticmethod
    def copy(source, start_index, length):
        """
        Copy the requested number of bytes from the start inside source to the
        output, one byte at a time.

        This is used for copy operations with overlapping input and output
        regions. Consider the following example where we will get ten copies
        of the string :code:`"ab"`:

        .. code-block:: python

           source = "ab"
           start_index = 0
           length = 20
           output = IncrementalCopy.copy(source, start_index, length)
           assert(output == "abababababababababab")
           assert(len(output) == length)

        .. note::

           This works slightly different compared to :code:`IncrementalCopy()`
           inside the :code:`gipfeli-internal.h` file of the reference
           implementation as we are returning **the new characters only**.

        :param source: The source string.
        :type source: str

        :param start_index: The index to start the copy operation at.
        :type start_index: int

        :param length: The number of characters to copy.
        :type length: int

        :return: The new data which has been copied from the source string.
        :rtype: str
        """
        if start_index >= len(source):
            raise ValueError("The start index is too large.")
        if length == 0:
            return ""

        # We have to init the output with the source string for the overlap
        # case.
        output = source
        requested_length = length
        index = start_index
        while True:
            output += output[index]
            index += 1
            length -= 1
            if length <= 0:
                break
        # Get only the new characters from the string.
        return output[-requested_length:]

    @staticmethod
    def copy_fast_path(source, start_index, length):
        """
        Copy the requested number of bytes from the start inside source to the
        output. This is equivalent to :func:`copy` except that it can write up
        to ten extra characters after the end of the copy, and that it is
        faster.

        The main part is a simple copy of eight bytes at a time until we have
        copied at least the requested amount of bytes. However, if the length
        of the source and the output differ in less than eight (indicating a
        repeating pattern of length < 8), we first need to expand the pattern
        in order to get the correct results. After performing a single copy
        of eight bytes from the source to the output, we have repeated the
        pattern once and we do not have to move the source index. This will be
        repeated until the two strings no longer overlap.

        This allows us to do very well in the special case of one single byte
        being repeated many times, without taking a big hit for more general
        cases.

        The worst case of extra writing past the end of the match occurs when
        the start index is at the last character of the source string and the
        length equals 1. Then the last copy will read from byte positions
        [0..7] and write to [4..11], whereas it was only supposed to write to
        position 1. For this reason, we use
        :attr:`MAX_INCREMENT_COPY_OVERFLOW`.

        .. note::

           This works slightly different compared to
           :code:`IncrementalCopyFastPath()` inside the
           :code:`gipfeli-internal.h` file of the reference implementation as
           we are returning **the new characters only**.

        :param source: The source string.
        :type source: str

        :param start_index: The index to start the copy operation at.
        :type start_index: int

        :param length: The number of characters to copy.
        :type length: int

        :return: The new data which has been copied from the source string.
        :rtype: str
        """
        if start_index >= len(source):
            raise ValueError("The start index is too large.")
        if length == 0:
            return ""

        output = source
        requested_length = length
        index = start_index

        while (len(output) - index) < 8:
            output += output[index : index + 8]
            length -= len(output) - index

        # Unlike in the reference implementation, we might have to perform an
        # additional iteration as it turned out during tests that we might get
        # incomplete results.
        # An alternative way would be to add an additional check which makes
        # sure that the length of the output is at least start_index +
        # requested_length characters long. But as this solution would require
        # some additional variables, this has not been chosen here.
        while length > -8:
            output += output[index : index + 8]
            index += 8
            length -= 8

        # Get only the new characters from the string. This is a bit tricky as
        # there might be some characters in the output afterwards. For this
        # reason, we just extract the needed characters, beginning at the given
        # start index.
        # Why do we have an assert here? - This is used to avoid that too short
        # strings are getting returned. In theory this should not be the case,
        # but as corresponding follow-up errors are harder to spot, we let it
        # fail here where the cause should be easier to spot. (Slicing strings
        # across its length will not throw an exception and silently return
        # less characters than requested.)
        return_value = output[start_index : start_index + requested_length]
        assert len(return_value) == requested_length
        return return_value
