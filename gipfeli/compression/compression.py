#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The main compression routines.
"""

from gipfeli.compression.entropy import Entropy
from gipfeli.compression.lz77 import LZ77


class Compressor:
    """
    The general class for performing compression using the Gipfeli algorithm.
    """

    _lz77 = None
    """
    The LZ77 instance to use.

    :type: :class:`gipfeli.compression.lz77.LZ77`
    """

    def __init__(self):
        self._reallocate_lz77()

    def _reallocate_lz77(self):
        """
        Reset the hash table of the used LZ77 instance. If it does not exist
        yet, we are creating a LZ77 instance.
        """
        if self._lz77:
            self._lz77.reset_hash_table()
        else:
            self._lz77 = LZ77()

    @staticmethod
    def _encode_original_length(length):
        """
        Get the number of bytes needed to store the original length and the
        encoded original length.

        :param length: The original data length of the uncompressed input.
        :type length: int

        :return: The number of bytes needed to encode the original data and the
                 length of the original data.
        :rtype: str
        """
        bytes_used = 0
        encoded_length = ""
        while length > 0:
            # 1 byte = 8 bit, so get the last byte from the length value and
            # save it.
            encoded_length += chr(length & 0xFF)
            # We have stored the lowest 8 bits (1 byte) from the length and can
            # calculate the remaining length now.
            length >>= 8
            # We stored the last 8 bits from the length.
            bytes_used += 1
        # The number of bytes used is the first value inside the output,
        # followed by the encoded length value using the corresponding number
        # of bytes.
        return chr(bytes_used) + encoded_length

    def compress(self, input_data):
        """
        Compress the given input data using the Gipfeli compression algorithm.

        Executed steps:

          1. Split the input into blocks of 64 kB.
          2. Execute the LZ77 algorithm on each of the blocks.
          3. Encode the commands and the content of each of the blocks using
             entropy coding.
          4. Merge all entropy coding outputs from the blocks into the final
             output.

        :param input_data: The data to compress.
        :type input_data: str

        :return: The compression result.
        :rtype: str
        """
        # Store the length of the input. First the number of bytes needed for
        # the length value, then the length value itself.
        compressed = self._encode_original_length(len(input_data))

        # Prepare the classes to call.
        self._reallocate_lz77()
        entropy = Entropy()

        # Save the input length to avoid having to call it multiple times.
        input_length = len(input_data)

        # Split input into BLOCK_SIZE sized blocks.
        position = 0
        while position < input_length:
            # Determine the start position of the previous block. A negative
            # value indicates that no previous block exists.
            previous_block_start = position - LZ77.BLOCK_SIZE if position else -1
            # Get the length of the fragment. At the end this is the number of
            # remaining characters.
            length = min(LZ77.BLOCK_SIZE, input_length - position)
            # Call the LZ77 compression.
            lz77_result = self._lz77.compress_fragment(
                input_data, position, length, previous_block_start
            )
            # Run entropy coding on it and add the output to the compressed
            # string.
            compressed += entropy.compress(lz77_result)
            # Move to the next block.
            position += length

        return compressed

    @staticmethod
    def get_max_compressed_length(number_of_bytes):
        """
        Get the upper bound for the compressed size of the input.

        The result is based on the following calculation:

          * Every emitted literal can bring 2 bits of overhead.
          * Every copy command saves more "worst case" bits than it costs:

            * Minimum savings is 4 literals of 10 bits, saving 4 * 10 bits.
            * Cost is 8 bits for an extra length command + 18 bits for the
              copy.

            This results in saving 40 - (18 + 8) = 14 bits.
          * However both the commands and literals are padded to 64 bits, so
            the switch could cost 64 bits due to worst case rounding (1 word
            extra for commands, none saved for literals). For large blocks this
            is not an issue, but it can be for smaller ones.

        :param number_of_bytes: The number of bytes inside the input.
        :type number_of_bytes: int

        :return: The maximum compressed length.
        :rtype: int
        """
        times = number_of_bytes // LZ77.BLOCK_SIZE
        full_blocks = times * Entropy.get_max_compressed_size(LZ77.BLOCK_SIZE, 1)
        number_of_bytes -= times * LZ77.BLOCK_SIZE
        partial_block = Entropy.get_max_compressed_size(number_of_bytes, 1) + 8
        max_length_size = 9
        return max_length_size + full_blocks + partial_block
