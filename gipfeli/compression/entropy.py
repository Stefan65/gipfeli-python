#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
All the methods needed for entropy coding.
"""

import copy

from gipfeli.common.bits import BitWriter
from gipfeli.common.utils import UnalignedDataAccess, Math
from gipfeli.compression.lz77 import CopyCommand, LiteralCommand


class EntropyResult:
    """
    Store for the result of entropy coding. This allows us to dig into the
    result for each of the parts of the output.
    """

    _commands_count = []
    _commands = []
    _bitmask = []
    _content = []

    def __init__(self):
        self._commands_count = []
        self._commands = []
        self._bitmask = []
        self._content = []

    @property
    def commands_count(self):
        """
        The number of commands which have been encoded.

        :type: :class:`list`
        """
        return self._commands_count

    @commands_count.setter
    def commands_count(self, count):
        self._commands_count = count

    @property
    def commands(self):
        """
        The commands created by the LZ77 algorithm.

        :type: :class:`list`
        """
        return self._commands

    @commands.setter
    def commands(self, commands):
        self._commands = commands

    @property
    def bitmask(self):
        """
        The bitmask used to encode the content.

        :type: :class:`list`
        """
        return self._bitmask

    @bitmask.setter
    def bitmask(self, mask):
        self._bitmask = mask

    @property
    def content(self):
        """
        The encoded content.

        :type: :class:`list`
        """
        return self._content

    @content.setter
    def content(self, data):
        self._content = data

    @staticmethod
    def __list_values_to_integer(list_object):
        """
        Convert the values of the given list to integer.

        This method is used to avoid having to write the list comprehension
        multiple times inside the :func:`as_list` method.

        :param list_object: The list to work on.
        :type list_object: list

        :return: The list with all the values converted to integer.
        :rtype: list[int]
        """
        return [ord(x) if isinstance(x, str) else x for x in list_object]

    def as_list(self):
        """
        Get the parts in the right order as a list of integer values.

        Order:

          * :attr:`commands_count`
          * :attr:`commands`
          * :attr:`bitmask`
          * :attr:`content`

        :return: All the parts of the encoded content as one big list.
        :rtype: list[int]
        """
        return (
            EntropyResult.__list_values_to_integer(self.commands_count)
            + EntropyResult.__list_values_to_integer(self.commands)
            + EntropyResult.__list_values_to_integer(self.bitmask)
            + EntropyResult.__list_values_to_integer(self.content)
        )

    def as_string(self):
        """
        Get the parts in the right order as a string. (See :func:`as_list` for
        the order.)

        :return: The parts of the encoded content as one string.
        :rtype: str
        """
        return "".join([chr(x) for x in self.as_list()])


class EntropyCodeBuilder:
    """
    Order symbols according to sampled data.

    We use

      * a code of 6 bit for the 32 most frequent values using the prefix
        :code:`0`,
      * a code of 8 bit for the next 64 values using the prefix :code:`10`,
      * a code of 10 bit for the other values using the prefix :code:`11`.
    """

    _limit32 = 0
    """
    The upper limit of where the 32 most frequent symbols end.

    :type: :class:`int`
    """

    _limit96 = 0
    """
    The upper limit of where the 32 most frequent and the 64 next values end
    and the remaining 160 begin.

    :type: :class:`int`
    """

    _choose_at_limit32 = 0
    """
    Specify how many symbols have to be taken into account for the 32 most
    frequent symbols when being at :attr:`_limit32` before switching to the
    next index group.

    :type: :class:`int`
    """

    _choose_at_limit96 = 0
    """
    Specify how many symbols have to be taken into account when being at
    :attr:`_limit96` before switching over to the last prefix group.

    :type: :class:`int`
    """

    def __init__(self):
        self._limit32 = 0
        self._limit96 = 0
        self._choose_at_limit32 = 0
        self._choose_at_limit96 = 0

    def find_limits(self, symbols):
        """
        Build a histogram and find the positions/limits where to split the
        symbols of the mask to the 32 most often used, the 64 less used and the
        other 160 even less used.

        :param symbols: The mask with the symbols to get the data from.
        :type symbols: list[int]
        """
        # Create a histogram where histogram[i] says how often i appears as the
        # value of the mask (how often this specific importance was used).
        histogram = [0] * 32
        for i in range(256):
            histogram[symbols[i]] += 1

        # Reset values.
        self._limit32 = -1
        self._limit96 = -1
        self._choose_at_limit32 = 0
        self._choose_at_limit96 = 0

        # Handle each of the sampler combinations. We start from the last list
        # index, because there the most samplers produce an output.
        count = 0
        for i in range(31, -1, -1):
            # Add the next sampler count.
            count += histogram[i]
            # Check if we discovered the 32 most frequent ones (for the first
            # time).
            if (count >= 32) and (self._limit32 == -1):
                # Save the limit which just is the current index inside the
                # histogram.
                self._limit32 = i
                # Determine how many of the occurrences from histogram[i] have
                # to be added to the first group before switching to the second
                # one.
                self._choose_at_limit32 = histogram[i] - (count - 32)
            # Check if we discovered the next 60 symbols (for the first time).
            if (count >= 96) and (self._limit96 == -1):
                # Save the limit which just is the current index inside the
                # histogram.
                self._limit96 = i
                if self._limit32 != self._limit96:
                    # Determine how many of the occurrences from histogram[i]
                    # have to be added to the second group before switching to
                    # the third one.
                    self._choose_at_limit96 = histogram[i] - (count - 96)
                else:
                    # If both limits are the same, set it to the maximal value.
                    self._choose_at_limit96 = 64

    def produce_symbol_order(self, symbols):
        """
        Build the symbol order and the entropy code using the limits computed
        by the :func:`find_limits` method.

        :param symbols: The mask with the symbols to run the comparisons with.
        :type: list[int]

        :return: The new value for each symbols in the first tuple element and
                 its bit length in the second one.
        :rtype: tuple[list[int], list[int]]
        """
        value_replacements = [0] * 256
        value_lengths = [0] * 256
        best_index = 0
        next_best_index = 0

        # Iterate over all symbols we are considering.
        for i in range(256):
            if symbols[i] >= self._limit32:
                # The current symbol is one of the most frequent ones.
                if symbols[i] == self._limit32:
                    # The value associated to the current symbol is at the
                    # border between the first and the second group. As long as
                    # we are having enough entries left for the current group,
                    # just decrease the counter as one less is available now.
                    self._choose_at_limit32 -= 1
                    if self._choose_at_limit32 == 0:
                        # We are just using the last value for the current
                        # group.
                        self._limit32 += 1
                # The 32 most frequent symbols use the prefix 0 (which we do
                # not have to write explicitly as every binary number can be
                # padded with 0 on the left) and 6 bits to encode the value.
                value_replacements[i] = best_index
                value_lengths[i] = 6
                # We have used one of the best (smallest) indices, so we have
                # to move to the next one.
                best_index += 1
                # As _limit96 is smaller than _limit32 we would enter the next
                # if, too. To avoid this, continue with the next symbol.
                continue
            if symbols[i] >= self._limit96:
                # The current symbol belongs to the second group.
                if symbols[i] == self._limit96:
                    # The value associated to the current symbol is at the
                    # border between the first and the second group. As long as
                    # we are having enough entries left for the current group,
                    # just decrease the counter as one less is available now.
                    self._choose_at_limit96 -= 1
                    if self._choose_at_limit96 == 0:
                        # We are just using the last value for the current
                        # group.
                        self._limit96 += 1
                # The next 64 symbols use the prefix 10 and 8 bits to encode
                # the value.
                # 0x80 = 10000000_2 (8 bits)
                value_replacements[i] = 0x80 | next_best_index
                value_lengths[i] = 8
                # We have used on of the best (smallest) indices, so we have to
                # move to the next one.
                next_best_index += 1
                # We do not want to encode this symbol again.
                continue
            # The last 160 symbols use the prefix 11 and 10 bits to encode
            # the value.
            # 0x300 = 1100000000_2 (10 bits)
            value_replacements[i] = 0x300 | i
            value_lengths[i] = 10

        return value_replacements, value_lengths


class Entropy:
    """
    Apply entropy coding compression to the output of LZ77 encoding. The output
    has to be at least :func:`get_max_compressed_size` long.
    """

    SAMPLERS = 5
    """
    We are using five samplers, which remember for each symbol if they have
    seen it or not.

    :type: :class:`int`
    """

    SAMPLE_PERIOD = 43
    """
    The sampler period to use.

    :type: :class:`int`
    """

    COPY_LENGTH_BITS = [
        12,
        12,
        12,
        12,
        12,
        12,
        12,
        12,
        12,
        12,
        15,
        15,
        15,
        18,
        18,
        18,
        13,
        13,
        13,
        13,
        13,
        13,
        13,
        13,
        13,
        13,
        19,
        19,
        19,
        19,
        19,
        19,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
        22,
    ]
    """
    The number of bits needed to encode the length and the offset/distance (sum
    of the number of bits).

    These values are listed in table 3 of the paper. The reference
    implementation defines this list inside the :code:`entropy.cc` file as
    :attr:`kLengthBits`.

    :type: :class:`list` [:class:`int`]
    """

    COPY_OFFSET_BITS = [
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        13,
        13,
        13,
        16,
        16,
        16,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
        16,
    ]
    """
    The number of bits needed to encode the offset/distance.

    These values are listed in table 3 of the paper. The reference
    implementation defines this list inside the :code:`entropy.cc` file as
    :attr:`kOffsetBits`.

    :type: :class:`list` [:class:`int`]
    """

    COPY_PREFIX = [
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        3,
        3,
        3,
        4,
        4,
        4,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        5,
        6,
        6,
        6,
        6,
        6,
        6,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
    ]
    """
    The decimal representation of the prefix.

    These values are listed in table 3 of the paper. The reference
    implementation defines this list inside the :code:`entropy.cc` file as
    :attr:`kBitsType`.

    :type: :class:`list` [:class:`int`]
    """

    COPY_LENGTH_PLUS = [4, 4, 4, 8, 4, 4, 4, 4]
    """
    Length offsets to use for copy commands. They are needed as the number of
    length bits is too small to represent the upper limit of the length value
    itself. Therefore we are shifting the upper limit of the length value to
    the maximum value which can be represented by the given number of length
    bits, resulting in the given shift values.

    The reference implementation defines this list inside the
    :code:`entropy.cc` file as :attr:`kLengthPlus`. They are not mentioned
    inside the paper.

    :type: :class:`list` [:class:`int`]
    """

    MASK_IMPORTANCE_ORDER = [
        0,
        3,
        2,
        7,
        2,
        7,
        5,
        11,
        2,
        7,
        5,
        11,
        5,
        11,
        9,
        14,
        1,
        6,
        4,
        10,
        4,
        10,
        8,
        13,
        4,
        10,
        8,
        13,
        8,
        13,
        12,
        15,
    ]
    """
    Gives the order of importance for masks (combination of samplers that found
    the symbol).

    The size is needed because of the way the :func:`count_samples` method
    works: For each symbol of the samples, we set a power of 2 from 2^0 to 2^4.
    For this reason, we have values with 5 bits - ranging from 0 (no sampler
    found the symbol) to 31 (all samplers found the symbol).

    The reference implementation defines this list inside the
    :code:`entropy.cc` file as :attr:`g_order`.

    :type: :class:`list` [:class:`int`]
    """

    SAMPLERS_HISTOGRAM = [
        33,
        81,
        86,
        93,
        145,
        157,
        161,
        177,
        239,
        266,
        270,
        312,
        387,
        492,
        717,
        1000,
    ]
    """
    Gives information on the relative estimate of the number of symbols given
    that combination of samplers when used together with
    :attr:`SAMPLERS_HISTOGRAM_15`.

    These values are shown inside table 6 of the paper. The reference
    implementation defines this list inside the :code:`entropy.cc` file as
    :attr:`histogram` in the :func:`Entropy::CountSamples` method.

    .. csv-table:: Overview of the mask value and their corresponding data
        :header: "Mask value", "Sampler combination", "Importance order"

        "00000", "∅", 0
        "00001", "L", 3
        "00010", "M", 2
        "00011", "ML", 7
        "00100", "M", 2
        "00101", "ML", 7
        "00110", "MM", 5
        "00111", "MML", 11
        "01000", "M", 2
        "01001", "ML", 7
        "01010", "MM", 5
        "01011", "MML", 11
        "01100", "MM", 5
        "01101", "MML", 11
        "01110", "MMM", 9
        "01111", "MMML", 14
        "10000", "S", 1
        "10001", "SL", 6
        "10010", "SM", 4
        "10011", "SML", 10
        "10100", "SM", 4
        "10101", "SML", 10
        "10110", "SMM", 8
        "10111", "SMML", 13
        "11000", "SM", 4
        "11001", "SML", 10
        "11010", "SMM", 8
        "11011", "SMML", 13
        "11100", "SMM", 8
        "11101", "SMML", 13
        "11110", "SMMM", 12
        "11111", "SMMML", 15

    :type: :class:`list` [:class:`int`]
    """

    SAMPLERS_HISTOGRAM_15 = 43000
    """
    This is the number of symbols that the last category :code:`SMMML` spotted,
    which means that the the symbols have been spotted by all five samplers.

    It gives information on the relative estimate of the number of symbols
    given that combination of samplers when used together with
    :attr:`SAMPLERS_HISTOGRAM`.

    These values are shown inside table 6 of the paper. The reference
    implementation defines this list inside the :code:`entropy.cc` file as
    :attr:`histogram` in the :func:`Entropy::CountSamples` method.

    :type: :class:`int`
    """

    def compress(self, lz77_output):
        """
        Compress the content and commands created as output of LZ77 encoding
        by using entropy coding.

        No entropy coding is used for the literals if the content size is too
        small. This is led by the fact that entropy coding brings an overhead
        of up to 44 bytes, but can save at most 25 % of the size of the
        content.

        :param lz77_output: The output of the LZ77 algorithm to compress.
        :type lz77_output: ~gipfeli.compression.lz77.Lz77Result

        :return: The content and commands compressed using entropy coding.
        :rtype: str
        """
        content = lz77_output.content
        content_size = len(content)
        commands = lz77_output.commands
        commands_size = len(commands)

        result = EntropyResult()

        mask = []
        use_entropy_code = False
        if content_size > 200:
            mask = self.sample_content(content)
            use_entropy_code = self.count_samples(mask)

        # Store the number of commands.
        result.commands_count = UnalignedDataAccess.unaligned_store_16(
            commands_size, "str"
        )

        # Store the commands.
        result.commands = self.compress_commands(commands)

        if use_entropy_code:
            # We decided to compress the data.

            # Replace the values of the mask with their importance.
            for i in range(256):
                mask[i] = self.MASK_IMPORTANCE_ORDER[mask[i]]

            # Build the entropy code itself. This creates a sort of conversion
            # table from the sampled data.
            code_builder = EntropyCodeBuilder()
            code_builder.find_limits(mask)
            value_replacements, value_lengths = code_builder.produce_symbol_order(mask)

            # Build the bitmask from the lengths.
            encoded_mask = self.build_entropy_code_bitmask(value_lengths)
            result.bitmask = encoded_mask

            # Compress the literals/content.
            bit_writer = BitWriter()

            # Every literal can be represented by at most 10 bits, so we can
            # always safely pack 6 of them at the same time to a 64 bit
            # integer.
            content_index = 0
            # Create the "full" packages, meaning that we have 6 literals for
            # each iteration.
            end_index = content_size // 6 + 1
            for _ in range(end_index - 1):
                character_value = ord(content[content_index])
                content_index += 1
                value = value_replacements[character_value]
                length = value_lengths[character_value]
                for i in range(5):
                    character_value = ord(content[content_index])
                    content_index += 1
                    value <<= value_lengths[character_value]
                    length += value_lengths[character_value]
                    value |= value_replacements[character_value]
                bit_writer.write(length, value)
            # Resume with the next entry.
            end_index = content_size - content_index + 1
            # Create the remaining packages, meaning the we have less than 6
            # literals for each iteration.
            for i in range(end_index - 1):
                character_value = ord(content[content_index])
                content_index += 1
                value = value_replacements[character_value]
                length = value_lengths[character_value]
                bit_writer.write(length, value)
            # Flush the writer to get the output.
            bit_writer.flush()
            result.content = bit_writer.output
        else:
            # We decided not to compress the data.
            # Store 0 as the bitmask = no entropy coding.
            result.bitmask = UnalignedDataAccess.unaligned_store_32(0, "str")

            # Copy the content directly to the output.
            result.content = content

        return result.as_string()

    @staticmethod
    def get_max_compressed_size(content_size, commands_size):
        """
        Get the maximum size the compressed content and commands can have.

        :param content_size: The size of the content.
        :type content_size: int

        :param commands_size: The size of the commands.
        :type commands_size: int

        :return: The maximum size the compressed data can have.
        :rtype: int
        """
        # Each block uses 16 bits (compare with explanations in lz77.LZ77
        # class).
        block_log = 16
        # Maximum number of bits used to encode a length of a copy command.
        length_log = 6
        # Bytes to encode the number of commands in a block.
        commands_length_size = 2
        # Maximum overhead of a literal-encoding table.
        max_literal_length_table_size = 48
        # Maximum bits to encode an "emit literal" command. This are 8 bits for
        # the prefix with a maximum of 63 and the remaining bits for the length
        # itself, which are 16 when using the prefix 63.
        max_literal_size = 8 + block_log
        # Maximum bits to encode an "emit copy" command. The 3 bits are for the
        # prefix.
        max_copy_size = 3 + length_log + block_log
        # Maximum bits to encode any command.
        max_command_size = max(max_literal_size, max_copy_size)
        # Maximum bits per byte of content.
        max_content_bits = 10
        # Words with 64 bit to encode the commands.
        command_words = (commands_size + max_command_size + 63) / 64
        # Words with 64 bits to encode the content.
        content_words = (content_size + max_content_bits + 63) / 64
        # Sum it up.
        return int(
            commands_length_size
            + max_literal_length_table_size
            + (command_words + content_words) * 8
        )

    def sample_content(self, input_data):
        """
        Sample the content to determine the approximation of its histogram.

        :param input_data: The data to work on.
        :type input_data: str

        :return: The created mask.
        :rtype: dict
        """
        # We are creating a mask with 512 elements here, but will use only the
        # the first 255 symbols (the ones in the ASCII table) later.
        mask = [0] * 512

        number_of_samples = len(input_data) // self.SAMPLE_PERIOD + 1
        input_index = 0

        def set_mask(value):
            """
            Set the mask value for the current input data index to the given
            value.

            This is a convenience method to avoid having to write/copy the
            assign operation each time.

            :param value: The value to perform the OR operation with.
            :type value: int
            """
            mask[ord(input_data[input_index])] |= value

        # Sample the input to 5 bits of the mask. Three samplers sample twice,
        # one sampler samples 3 times and one sampler 1 time per period.
        # This uses the same code as the reference implementation.
        for _ in range(number_of_samples - 1):
            set_mask(1)
            input_index += 2
            set_mask(2)
            input_index += 5
            set_mask(4)
            input_index += 6
            set_mask(8)
            input_index += 3
            set_mask(16)
            input_index += 7
            set_mask(1)
            input_index += 2
            set_mask(2)
            input_index += 5
            set_mask(4)
            input_index += 6
            set_mask(8)
            input_index += 3
            set_mask(1)
            input_index += 4

        return mask

    def count_samples(self, mask):
        """
        Build histogram and determine whether to do entropy coding of content.

        For the first 32 symbols we save 2 bits per symbol if we decide to
        apply entropy coding. On the other hand from symbols 96 onwards, we
        loose 2 bits per symbol. We decide whether to apply compression
        depending on their proportion (where the estimation of proportion is
        biased towards not doing compression).

        :param mask: The mask to use for it.
        :type mask: dict

        :return: Whether to perform entropy coding or not.
        :rtype: bool
        """
        # The value characters_count[i] represents the number of symbols in the
        # i-th category of the histogram (the amount of how often the
        # corresponding sampler occurred).
        characters_count = [0] * 16
        for i in range(254, 0, -1):
            characters_count[self.MASK_IMPORTANCE_ORDER[mask[i]]] += 1
        if characters_count[self.MASK_IMPORTANCE_ORDER[mask[0]]] != 255:
            characters_count[self.MASK_IMPORTANCE_ORDER[mask[0]]] += 1

        # Relative estimation on the number of symbols in the input given that
        # combination of samplers corresponding to i-th order of occurrence.
        histogram = copy.deepcopy(self.SAMPLERS_HISTOGRAM)
        histogram_15 = self.SAMPLERS_HISTOGRAM_15
        # Reduce the last value by the previous ones.
        for i in range(15):
            histogram_15 -= histogram[i] * characters_count[i]
        # Adapt the last value according to the characters count.
        if characters_count[15] > 0:
            histogram_15 //= characters_count[15]
            histogram[15] = max(histogram_15, 1000)

        # Determine the proportion of the 32 most frequent symbols.
        proportion_first_32 = 0
        to_add = 32
        i = 15
        # Repeat until we have seen 32 symbols, starting from the last list
        # index, because there the most samplers found the character.
        while to_add > 0:
            proportion_first_32 += histogram[i] * characters_count[i]
            to_add -= characters_count[i]
            i -= 1
        # Correct the value by reducing it with a multiple of the last used
        # histogram value (we decrease the counter in the last step of the
        # loop, so we have to increase it here to get the ones we used at
        # last). By doing so, we get exactly the first 32 symbols, as to_add
        # equals 0 afterwards.
        proportion_first_32 += histogram[i + 1] * to_add

        # Determine the proportion of the last 160 symbols (the least
        # frequent).
        proportion_from_96 = 0
        to_add = 160
        i = 0
        # Repeat until we have seen 160 symbols, starting from the first list
        # index, because there the fewest samplers found the character.
        while to_add > 0:
            proportion_from_96 += histogram[i] * characters_count[i]
            to_add -= characters_count[i]
            i += 1
        # Correct the value by reducing it with a multiple of the last used
        # histogram value (we increase the counter in the last step of the
        # loop, so we have to decrease it here to get the ones we used at
        # last). By doing so, we get exactly the last 160 symbols, as to_add
        # equals 0 afterwards.
        proportion_from_96 += histogram[i - 1] * to_add

        # 352 bits = 44 bytes is a penalty for overhead needed to encode table
        # of entropy code.
        return proportion_first_32 * 6 + 352 > proportion_from_96 * 10

    def build_entropy_code_bitmask(self, value_lengths):
        """
        Build entropy coding bitmask to transfer the encoding of symbols.

        :param value_lengths: The number of bits to use for each of the
                              symbols.
        :type value_lengths: list[int]

        :return: The mask itself.
        :rtype: str
        """
        # The maximum length of the mask can be 4 + 32 + 12 bytes.
        mask = [0] * 48

        # First save mask of 6 and 8 bit long symbols in two levels. We split
        # 256 symbols to 32 segments of length 8. First 32 bits (4 bytes) will
        # encode if the segment is not empty. Then for each non-empty segment
        # we use 8 bits to encode which symbols are present. The overall length
        # of this part is 36 bytes (4 bytes for the general indication + 1 byte
        # for each of the 32 segments for the present symbols).
        mask_index = 0
        non_empty = 0
        for i in range(4):
            # Handle the current segment group which consists of 8 segments.
            mask[mask_index] = 0
            for j in range(8):
                # Handle the current segment.
                present = 0
                value = 0
                # The current segment contains 8 symbols.
                for k in range(8):
                    if value_lengths[64 * i + 8 * j + k] <= 8:
                        # The current symbol is inside the first two groups.
                        present = 1
                        # Set the (7-k)-th index of the binary value
                        # representation to 1.
                        value |= 1 << (7 - k)
                # Update the not-empty indicator for the current segment.
                # Set the (7-j)-th index of the binary value representation
                # to 1 (not empty) or 0 (empty).
                mask[mask_index] |= present << (7 - j)

                # This segment is not empty.
                if present:
                    # Use the next free entry to indicate which of the 8
                    # symbols of the current segment are inside the first two
                    # groups. We have an offset of 4 because the first 4
                    # entries encode if the segment is empty or not.
                    mask[4 + non_empty] = value
                    # We have filled one more list entry.
                    non_empty += 1

            # Move to the next entry to save information about the emptiness.
            mask_index += 1

        # Move the mask index to the next empty list entry.
        mask_index += non_empty

        # In the second phase we encode which symbols have length 6 as a subset
        # bitmask from count bits, where 96 is the number of symbols having 6
        # or 8 bit codes. Overall we use 12 bytes for this part.
        used_bits = 0
        mask[mask_index] = 0
        for i in range(256):
            if value_lengths[i] <= 8:
                # The current symbol belongs to one of the first two groups.
                # Make room for the next value.
                mask[mask_index] <<= 1
                # Save 1 if the current symbol belongs to the first group, save
                # 0 otherwise.
                mask[mask_index] |= value_lengths[i] == 6
                # We wrote one more bit.
                used_bits += 1
                if used_bits == 8:
                    # We wrote one byte. Move to the next list index.
                    used_bits = 0
                    mask_index += 1
                    if mask_index >= 48:
                        # We have reached the last list index, so we will not
                        # have more data for the mask.
                        break
                    mask[mask_index] = 0

        # Remove the last zeros, caused by the empty segments, from the
        # bitmask. 32 is the maximum number of non-empty segments possible.
        empty = 32 - non_empty
        mask = mask[:-empty]

        # Build a string from the mask.
        return "".join([chr(x) for x in mask])

    def compress_commands(self, commands):
        """
        Write the commands to the output compressed as values of variable
        length.

        .. note::

           It adds about 10 to 15 % compression improvement to represent
           commands, which is important, because after applying LZ77 to
           text or HTML, around half of the data can be commands (according
           to analyses).

        Format of compressed commands:

          * Literals start with :code:`00` followed by (the corresponding
            length - 1) (because we do not have regular zero-length literals)
            using

            * 6 bits if the length is less than 53,
            * (length - 48) bits otherwise.
          * Copies start with :code:`010`, :code:`011`, :code:`100`,
            :code:`101`, :code:`110` or :code:`111`. Each of them corresponds
            to a specific number of offset and length bits, based on
            :attr:`COPY_LENGTH_BITS`, :attr:`COPY_BITS_PREFIX`,
            :attr:`COPY_LENGTH_PLUS` and :attr:`COPY_OFFSET_BITS`.

        :param commands: The commands to compress. There always should be at
                         least one.
        :type commands: list[~gipfeli.compression.lz77.Lz77Command]

        :return: The compressed commands.
        :rtype: list[str]
        """
        bit_writer = BitWriter()

        for command in commands:
            if isinstance(command, LiteralCommand):
                # Literal command, so we have to encode the length value.
                length = command.length - 1
                if length < 53:
                    # Write 00length, using 8 bits in total.
                    bit_writer.write(8, length)
                else:
                    # Get the number of bits needed to encode the length.
                    bit_length = Math.log2_floor_non_zero(length) + 1
                    # Write 47 + bit_length, then the length itself. The offset
                    # of 8 is needed for the 1 byte specifying the bit length
                    # (with 63 being the highest value possible for 47 +
                    # bit_length).
                    value = ((47 + bit_length) << bit_length) | length
                    bit_writer.write(8 + bit_length, value)
            elif isinstance(command, CopyCommand):
                # Backward reference, so we have to encode the length and the
                # offset. The offset is reduced by one as an offset of 0 is not
                # possible as output of the LZ77 run.
                length = command.length
                offset = command.offset - 1
                # Get the number of bits needed to encode the length value
                # as-is. Then determine the index to use inside the lists
                # storing the information about the static entropy code.
                bits_length = Math.log2_floor_non_zero(length)
                index = (bits_length - 2) * 16 + Math.log2_floor(offset | 1)

                # Retrieve the values from the lists.
                prefix = self.COPY_PREFIX[index]
                data_length = self.COPY_LENGTH_BITS[index]
                length_plus = self.COPY_LENGTH_PLUS[bits_length]
                offset_length = self.COPY_OFFSET_BITS[index]

                # Build the value needed to encode the copy command. Thinking
                # in binary mode, we just do the following: write the 3 prefix
                # bits, append the copy length (minus an offset value) using
                # the right number of bits (2 to 6), then append the
                # offset/distance value using the right number of bits (10 to
                # 16).
                value = (
                    (prefix << data_length)
                    | (length - length_plus) << offset_length
                    | offset
                )
                # Write the value. We need to add 3 to the data length for the
                # 3 prefix bits.
                bit_writer.write(data_length + 3, value)

        # We always have to flush the writer to avoid data loss. Without this,
        # the last 64 bits = 8 bytes would be missing from the output.
        bit_writer.flush()
        return bit_writer.output
