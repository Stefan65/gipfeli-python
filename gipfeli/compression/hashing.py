#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
A collection of functionality concerning hashing.
"""

from gipfeli.common.utils import UnalignedDataAccess


class HashTable:
    """
    A basic implementation of a hash table.

    .. note::

       As tests have shown that using plain integer values as the index of a
       list for a hash table does not work as the values are too large, we
       have to use a different approach here. In this implementation, we are
       using a dictionary instead of a list which acts as the table and
       convert all index values to a string before accessing the dictionary.

    .. note::

       The reference implementation uses a hash table with values consisting of
       16 bits. This goes in conjunction with the paper where the authors are
       writing about the LZ77 algorithm using a sliding window of 16 bit.
    """

    _table = {}
    """
    The hash table itself.

    :type: :class:`list`
    """

    HASH_MULTIPLICATOR = 0x1E35A7BD
    """
    The multiplicator to use for hashing.

    This has been taken from the reference implementation where it is defined
    inside the :func:`HashBytes` method of the :code:`lz77.cc` file.

    :type: :class:`int`
    """

    UINT16_UPPPER_BOUND = 65536
    """
    This is the smallest value above the upper bound of an unsigned integer
    value of 16 bits in the C/C++ context.

    The range of an :code:`uint16_t` value is
    :code:`[0, 65535] = [0, 65536) = [0, 2^16)`. To correctly map values
    exceeding this limits to the correct value in conjunction with the
    reference implementation, we just have to subtract this constant from the
    value if it is greater or equal to it. This is used as Python does not
    provide explicit typing, but normally our values should not exceed this
    limit.

    :type: :class:`int`
    """

    def __init__(self):
        self._table = {}

    def hash_bytes(self, bytes_value, shift=0):
        """
        Hash the given bytes value. According to the reference implementation,
        this are 4 bytes = 4 characters.

        .. note::

           Any hash function will produce a valid compressed bitstream, but a
           good hash function reduces the number of collisions and thus yields
           better compression for compressible input, and more speed for
           incompressible input. Of course, it does not hurt if the hash
           function is reasonably fast either, as it gets called a lot.

        :param bytes_value: The value representing the 4 bytes to be hashed.
        :type bytes_value: int

        :param shift: The shift to apply.
        :type shift: int

        :return: The calculated hash.
        :rtype: int
        """
        return (bytes_value * self.HASH_MULTIPLICATOR) >> shift

    def hash(self, data, shift=0):
        """
        Hash the first 4 bytes/characters of the given input data.

        :param data: The input to get the first 4 bytes from.
        :type data: str

        :param shift: The shift to apply.
        :type shift: int

        :return: The calculated hash for the first 4 bytes.
        :rtype: int
        """
        return self.hash_bytes(
            UnalignedDataAccess.unaligned_load_32(data, return_type="int"), shift
        )

    def set(self, index, value):
        """
        Set the hash table entry at the given index to the given value.

        .. note::

           The given value will always be mapped to a :code:`uint16_t` value
           ranging from 0 to 65535.

        :param index: The index to set the value for.
        :type index: str or int

        :param value: The value to set at the index.
        :type value: int
        """
        if isinstance(index, int):
            index = str(index)
        if value >= self.UINT16_UPPPER_BOUND:
            value -= self.UINT16_UPPPER_BOUND
        self._table[index] = value

    def get(self, index, default=0):
        """
        Get the hash table entry at the given index.

        .. note::

           As we do not create a hash table with the needed indices on
           :func:`__init__`, we use the default value to return a result for
           unknown indices.

        :param index: The index to get the value for.
        :type index: str or int

        :param default: The default value to use if there is no value for the
                        given index.
        :type default: int

        :return: The requested value if the index is known, the default value
                 otherwise.
        :rtype: int
        """
        if isinstance(index, int):
            index = str(index)
        return self._table.get(index, default)

    def reset(self):
        """
        Reset the hash table. This just assigns a new dictionary.
        """
        self._table = {}

    def __repr__(self):
        return self._table.__repr__()


class UnhashedTable(HashTable):
    """
    A special implementation of a hash table whose hashing functions are just
    returning the original data as a string.

    .. note::

       This class can be used to make looking at the hash table values easier -
       knowing what hashed integer values mean can be hard when trying to
       understand the LZ77 algorithm better, while strings with the "real"
       content should not cause any trouble.

    .. warning::

       While :class:`~gipfeli.compression.hashing.HashTable` can have a maximum
       of 2^15 entries due to the used hash function, this implementation
       allows the growth of the hash table beyond this value (all combinations
       of strings with four characters). This might not be a problem for
       experimental usage like the one this implementation has been written
       for, but should be taken into account for real-world usage.
    """

    def hash_bytes(self, bytes_value, shift=0):
        """
        Hash the first 4 bytes/characters of the given input data.

        If the input is a string, we just return it as-is. Otherwise we convert
        the given integer to a string using the corresponding method for
        unaligned storage.

        :param bytes_value: The 4 bytes to be hashed.
        :type bytes_value: str or int

        :param shift: The shift to apply. This variable is unused and can be
                      set to any value.
        :type shift: int

        :return: The 4 characters from the input in the string case, the
                 corresponding 4 characters from the input in the integer case.
        :rtype: str
        """
        if isinstance(bytes_value, str):
            return bytes_value[0:4]
        return UnalignedDataAccess.unaligned_store_32(bytes_value, return_type="str")

    def hash(self, data, shift=0):
        """
        Get the first 4 characters of the input.

        :param data: The input to get the first 4 characters from.
        :type data: str

        :param shift: The shift to apply. This variable is unused and can be
                      set to any value.
        :type shift: int

        :return: The first 4 characters of the input data.
        :rtype: str
        """
        return data[0:4]
