#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This class is responsible for handling the execution of the LZ77 algorithm used
to convert the input into a sequence of literal commands and copy commands
(backward references).
"""

from abc import abstractmethod

from gipfeli.common.utils import UnalignedDataAccess
from gipfeli.compression.hashing import UnhashedTable, HashTable
from gipfeli.compression.matching import Matcher


class Lz77Command:
    """
    Base class for every command.

    Each of the subclasses needs to implement

    * the :func:`__str__` method to allow printing the contained data without
      much additional code for example.
    """

    @abstractmethod
    def __str__(self):
        pass


class LiteralCommand(Lz77Command):
    """
    Data store for a literal command.

    This is used if the corresponding character sequence has not been seen
    before.

    The string representation of this command is just the length as a string.
    """

    _length = 0

    def __init__(self, length):
        self.length = length

    @property
    def length(self):
        """
        The length of the literal aka the number of characters.

        .. note::

           When setting a new length, this has to be greater than zero as
           emitting empty literals does not make sense at all. The value will
           not be updated if the new length is too small.

        :type: :class:`int`
        """
        return self._length

    @length.setter
    def length(self, new_length):
        if new_length > 0:
            self._length = new_length

    def __str__(self):
        return str(self.length)


class CopyCommand(Lz77Command):
    """
    Data store for a copy command.

    This is used if the corresponding character sequence has been seen before.
    It gets called a "backward reference" in some cases, too.

    The string representation of this command is a tuple of the offset and the
    length.
    """

    _length = 0
    _offset = 0

    def __init__(self, offset, length):
        self.offset = offset
        self.length = length

    @property
    def length(self):
        """
        The length of the data to copy aka the number of characters.

        .. note::

           When setting a new length, this has to be greater or equal to four.
           This is driven by the fact that the reference implementation is
           searching for matches which are (at least) 4 bytes/characters long.
           (This is implicitly mentioned inside the original paper on site 6,
           too, in the context of some writeup about the hash table entries.)
           The value of the variable will not be updated if the new length is
           too small.

        :type: :class:`int`
        """
        return self._length

    @length.setter
    def length(self, new_length):
        if new_length >= 4:
            self._length = new_length

    @property
    def offset(self):
        """
        The offset to determine where the copy command starts its action.

        .. note::

           When setting a new offset, this has to be greater then zero as a
           value of zero is not really an offset as it references the current
           position (and negative offsets would reference future outputs we
           cannot know at the moment). The value will not be updated if the new
           offset is too small.

        :type: :class:`int`
        """
        return self._offset

    @offset.setter
    def offset(self, new_offset):
        if new_offset >= 1:
            self._offset = new_offset

    def __str__(self):
        return str(tuple([self.offset, self.length]))


class Lz77Result:
    """
    Data class to store the results of running the LZ77 algorithm.

    The string representation of this is the string representation of the
    command list, followed by the content and divided by a space.
    """

    _commands = []
    _content = ""

    def __init__(self):
        self._commands = []
        self._content = ""

    @property
    def commands(self):
        """
        The sequence of commands produced by the LZ77 algorithm.

        :type: :class:`list` of :class:`~gipfeli.compression.lz77.Lz77Command`
        """
        return self._commands

    @property
    def content(self):
        """
        The content (literals) produced by the LZ77 algorithm.

        :type: :class:`str`
        """
        return self._content

    def append_command(self, command):
        """
        Append a command to the list of commands.

        :param command: The command to append.
        :type command: :class:`~gipfeli.compression.lz77.Lz77Command`
        """
        self._commands.append(command)

    def append_content(self, string):
        """
        Append a string to the content.

        :param string: The string to append.
        :type string: :class:`str`
        """
        self._content += string

    def __str__(self):
        return (
            "{} "
            "{}"
            "".format(str([str(x) for x in self.commands]), str(self.content))
        )


class LZ77:
    """
    Perform compression using backward references.

    Example usage:

    .. code-block:: python

       data = "123456890"
       lz77 = LZ77()
       result = lz77.compress_fragment(data)
       content = result.content
       commands = result.commands
    """

    _hash_table = None
    """
    The hash table to use.

    :type: `class:`gipfeli.compression.hashing.HashTable`
    """

    BLOCK_SIZE = 1 << 16
    """
    The block size to use. This equals 2^16.

    The compression code in
    :func:`gipfeli.compression.compression.Compressor.compress`
    chops up the input into blocks of at most :attr:`BLOCK_SIZE`. Additionally
    we use back-references up to :attr:`BLOCK_SIZE` - by having the sliding
    window approach which allows us to back-reference also to previous blocks.

    :type: :class:`int`
    """

    INPUT_MARGIN_BYTES = 15
    """
    The maximum number of remaining bytes before emitting the remaining bytes
    of the input as a literal.

    This is set in conjunction with the reference implementation inside the
    :code:`lz77.cc` file.

    :type: :class:`int`
    """

    def __init__(self):
        self.reset_hash_table()

    def reset_hash_table(self):
        """
        Reset the hash table. If it does not exist yet, we are creating one.

        .. warning::

           This uses :class:`~gipfeli.compression.hashing.UnhashedTable` by
           default. Make sure to read the warning for this class and switch to
           :class:`~gipfeli.compression.hashing.HashTable` when creating the
           class instance here if needed.
        """
        if self._hash_table:
            self._hash_table.reset()
        else:
            self._hash_table = UnhashedTable()

    def compress_fragment(
        self, input_data, start_position, fragment_length, previous_block_start
    ):
        """
        Compress the input string.

        Steps:

          1. Scan forward in the input looking for a 4-byte-long match. If we
             get close to exhausting the input then emit the remainder.
          2. A 4-byte match has been found. Emit the literal bytes.
          3. Create a copy command and then check if another copy command can
             be used. Repeat until we find no match for the input immediately
             after what was used for the last copy call.

        During the first step, we use heuristic match skipping: If 32 bytes are
        scanned without any matches, we start looking only at every other byte.
        If 32 more bytes are scanned, look at every third byte, and so on. When
        a match is found, we immediately go back to looking at every byte.
        While this is a small loss (around 5 % performance, around 0.1 %
        density) for compressible data due to more bookkeeping, it is a huge
        win for non-compressible data - such as JPEG - as the compressor
        quickly gets aware of the fact that the data is incompressible and does
        not bother looking for matches everywhere.

        :param input_data: The data to compress. This has to be at most
                           :attr:`BLOCK_SIZE` bytes/characters long.
        :type input_data: str

        :param start_position: The start index of the current block.
        :type start_position: int

        :param fragment_length: The length of the fragment.
        :type fragment_length: int

        :param previous_block_start: The start index of the previous block. If
                                     the value is negative, no previous block
                                     has to be considered (probably because it
                                     does not exist).
        :type previous_block_start: int

        :return: The compression result.
        :rtype: Lz77Result
        """
        result = Lz77Result()

        # Set the start and the end position.
        input_position = start_position
        input_end = start_position + fragment_length

        # All the bytes in [next_emit, input_position) will be emitted as
        # literal bytes - or [next_emit, ip_end) at the end.
        next_emit_position = input_position

        # In the - very unlikely - case that the input is very small, we use
        # just one literal command.
        if fragment_length < self.INPUT_MARGIN_BYTES:
            return self.emit_remainder(
                result, input_data, next_emit_position, input_end
            )

        # The index limit when to call the _emit_remainder method.
        input_position_limit = input_end - self.INPUT_MARGIN_BYTES

        # Get the first hash. This is placed inside the initialization of the
        # for-loop of the reference implementation.
        input_position += 1
        next_hash = self._hash_table.hash(input_data[input_position:])

        # This is the equivalent of the for-loop of the reference
        # implementation. There is no return statement after it as all the
        # returns are happening inside the body when only a small amount of
        # characters remains.
        while True:
            # We emit an literal once and then create at least one copy command
            # (in the best case more than once). When we are close to
            # exhausting the input, we emit the remainder.
            #
            # In the first iteration we have just started and there is nothing
            # to copy. For this reason we have to emit a literal once.
            # We will not start a new iteration before the current iteration
            # determined that emitting a literal in the new step will lead to
            # the next call of a copy command (if any).

            ##########
            # Step 1 #
            ##########

            # The skip variable keeps track of how many bytes there are since
            # the last match. Dividing it by 32 (right-shifting it by five)
            # gives the number of bytes to move ahead for each iteration.
            # By using a value of 32 at the beginning, we start with the
            # regular step size we would use otherwise, too.
            # But why are we dividing by 32 (according to the reference
            # implementation? - There is no reason given for this. This might
            # be led by the fact that we are searching for matches of a least
            # 4 bytes = 32 bits, but this is more a speculation.
            skip = 32

            next_position = input_position
            # Walk through the input from the current position on until we
            # found a match of at least 4 bytes.
            while True:
                input_position = next_position
                current_hash = next_hash
                # Determine how many bytes to skip and get the next index.
                bytes_between_hash_lookups = skip >> 5
                skip += 1
                next_position = input_position + bytes_between_hash_lookups
                # We do not have enough input data remaining.
                if next_position > input_position_limit:
                    return self.emit_remainder(
                        result, input_data, next_emit_position, input_end
                    )
                # Check if we already know the current 4 bytes and get the
                # corresponding offset. This occurrence might have been inside
                # the previous block, so check and handle this.
                next_hash = self._hash_table.hash(input_data[next_position:])
                offset = self._hash_table.get(current_hash)
                candidate_position = start_position + offset
                if candidate_position >= input_position:
                    candidate_position = previous_block_start + offset
                    in_previous_block = True
                else:
                    in_previous_block = False

                # Our current value can be saved now for lookups later.
                self._hash_table.set(current_hash, input_position - start_position)

                # The loop exit condition: We have found a match which is (at
                # least) 4 bytes long. We will determine if the match is more
                # than 4 bytes later.
                # If we grabbed an old offset from the hash table, this
                # condition will evaluate to false and we are moving the input
                # pointer forward by one position.
                if UnalignedDataAccess.unaligned_load_32(
                    input_data[input_position:]
                ) == UnalignedDataAccess.unaligned_load_32(
                    input_data[candidate_position:]
                ):
                    break

            ##########
            # Step 2 #
            ##########

            # The input bytes [next_emit, ip) are unmatched. Emit them.
            self.emit_literal(
                result,
                input_data[next_emit_position:],
                input_position - next_emit_position,
            )

            ##########
            # Step 3 #
            ##########

            # If we exit this loop normally then we need to emit a literal
            # next, though we do not yet know how big the literal will be. This
            # is handled by proceeding to the next iteration of the main loop.
            # We can exit this loop when getting close to exhausting the input,
            # too.
            while True:
                # We already know that we have a 4 byte long match at the input
                # position and do not have to emit any literal bytes prior to
                # the input position.
                base_position = input_position

                # Check how many bytes really match.
                matched = 4
                if (
                    not in_previous_block
                    or ((previous_block_start + self.BLOCK_SIZE) == start_position)
                    or (
                        candidate_position + 4
                        >= (previous_block_start + self.BLOCK_SIZE)
                    )
                ):
                    matched += Matcher.find_match_length(
                        input_data,
                        candidate_position + 4,
                        input_position + 4,
                        input_end,
                    )
                else:
                    matched += Matcher.multi_block_find_match_length(
                        input_data,
                        candidate_position + 4,
                        previous_block_start + self.BLOCK_SIZE,
                        start_position,
                        input_position + 4,
                        input_end,
                    )

                # Avoid overflows of the length. This is needed as a copy
                # command does not support lengths greater than 67.
                matched = min(matched, 67)

                # We can move the input pointer to the end of the match.
                input_position += matched

                # Check whether the match is in a previous block and handle it
                # accordingly.
                if in_previous_block:
                    offset = (
                        base_position
                        - start_position
                        + previous_block_start
                        + self.BLOCK_SIZE
                        - candidate_position
                    )
                else:
                    offset = base_position - candidate_position

                # Emit the command.
                self.emit_copy(result, offset, matched)

                # Update the hash table at the end of the current input, namely
                # input_position - 1, ..., input_position - 3.
                insert_tail = input_position - 3
                next_emit_position = input_position
                if input_position >= input_position_limit:
                    return self.emit_remainder(
                        result, input_data, next_emit_position, input_end
                    )
                input_bytes = UnalignedDataAccess.unaligned_load_64(
                    input_data[insert_tail:], "int"
                )

                n = insert_tail - start_position
                for offset in range(3):
                    previous_hash = self._hash_table.hash_bytes(
                        self._get_int_at_offset(input_bytes, offset)
                    )
                    self._hash_table.set(previous_hash, n)
                    n += 1
                current_hash = self._hash_table.hash_bytes(
                    self._get_int_at_offset(input_bytes, 3)
                )
                candidate_offset = self._hash_table.get(current_hash)
                candidate_position = start_position + candidate_offset
                if candidate_position >= input_position:
                    candidate_position = previous_block_start + candidate_offset
                    in_previous_block = True
                else:
                    in_previous_block = False
                self._hash_table.set(current_hash, n)

                # The loop exit condition: We could not find another sequence
                # we could copy.
                if UnalignedDataAccess.unaligned_store_32(
                    self._get_int_at_offset(input_bytes, 3)
                ) != UnalignedDataAccess.unaligned_load_32(
                    input_data[candidate_position:], "str"
                ):
                    break

            # Calculate the next hash and move to the next index afterwards.
            next_hash = self._hash_table.hash_bytes(
                self._get_int_at_offset(input_bytes, 4)
            )
            input_position += 1

    @staticmethod
    def _get_int_at_offset(value, offset):
        """
        Get the integer at the given offset.

        This has been taken from the reference implementation, namely
        :func:`GetUint32AtOffset` inside the :code:`lz77.cc` file. According to
        the documentation of the method, this method is used to improve the
        performance. Their motivation: They have empirically found that
        overlapping loads like unaligned_load_32(p) ...
        unaligned_load_32(p + 1) ... unaligned_load_32(p + 2) are slower than
        unaligned_load_64(p) followed by shifts.

        .. warning::

           This only considers the little endian version and you might
           encounter errors on big endian machines. See
           :class:`~gipfeli.common.utils.UnalignedDataAccess` for some more
           words about the endianness, too.

        :param value: The value to work on.
        :type value: int

        :param offset: The shift to perform.
        :type offset: int

        :return: The shifted value.
        :rtype: int
        """
        return value >> (8 * offset)

    @staticmethod
    def emit_literal(result, data_from_next_emit_position_on, length):
        """
        Emit a literal of the given length.

        :param result: The result instance to append the data to.
        :type result: Lz77Result

        :param data_from_next_emit_position_on: The input data from the next
                                                emit position on. Starting from
                                                the beginning of this, we are
                                                going to get the literal.
        :type data_from_next_emit_position_on: str

        :param length: The length of the literal to emit.
        :type length: int

        :return: The result instance.
        :rtype: Lz77Result
        """
        result.append_command(LiteralCommand(length))
        result.append_content(data_from_next_emit_position_on[0:length])
        return result

    @staticmethod
    def emit_copy(result, offset, length):
        """
        Create a copy command with the given parameters.

        :param result: The result instance to append the data to.
        :type result: Lz77Result

        :param offset: The offset to set.
        :type offset: int

        :param length: The length to set.
        :type length: int

        :return: The result instance.
        :rtype: Lz77Result
        """
        result.append_command(CopyCommand(offset, length))
        return result

    @staticmethod
    def emit_remainder(result, input_data, next_emit_position, input_end):
        """
        Emit the remaining data of the input, namely everything inside of
        :code:`[next_emit_position, ip_end)`.

        :param result: The result instance to append the data to.
        :type result: Lz77Result

        :param input_data: The input data to get the literal from.
        :type input_data: str

        :param next_emit_position: The next emit position. This is the start
                                   of the literal to emit.
        :type next_emit_position: int

        :param input_end: The last index to use from the data. This is the
                          end of the literal to emit.
        :type input_end: int

        :return: The result instance.
        :rtype: Lz77Result
        """
        if next_emit_position < input_end:
            result = LZ77.emit_literal(
                result,
                input_data[next_emit_position:],
                abs(input_end - next_emit_position),
            )
        return result

    @staticmethod
    def get_max_compressed_content_size(input_size):
        """
        Get the maximum size which the compressed content could have.

        As the content will always be a sub-sequence of the input, the worst
        case is that the content size equals the input size.

        :param input_size: The size of the input.
        :type input_size: int

        :return: The maximum size of the content after compression.
        :rtype: int
        """
        return input_size

    @staticmethod
    def get_max_compressed_commands_size(input_size):
        """
        Get the maximum size which the compressed commands could have.

        Th worst case scenario is when these two commands are repeated all the
        time:

          * emit literal of length 1 (use 1 command for it),
          * copy of length 4 (use 1 command for it).

        So we get at most 2 commands for each 5 symbols of the input. The
        exception when we need more commands than the input size is an input
        size of exactly 1, when we need 1 command.

        We need one extra command reserved because
        :func:`gipfeli.compression.entropy.Entropy.compress_commands`
        pre-fetches the next one before the comparison on array size.

        :param input_size: The size of the input.
        :type input_size: int

        :return: The maximum size of the commands after compression.
        :rtype: int
        """
        return 2 * input_size // 5 + 2

    @staticmethod
    def decompress(encoded):
        """
        Recreate the original content using the results of
        func:`compress_fragment`. This can be used to verify the right
        behaviour.

        :param encoded: The encoded data.
        :type encoded: Lz77Result

        :return: The recreated uncompressed string.
        :rtype: str
        """
        content = encoded.content
        content_pos = 0
        output = ""
        for command in encoded.commands:
            if isinstance(command, LiteralCommand):
                output += content[content_pos : content_pos + command.length]
                content_pos += command.length
            elif isinstance(command, CopyCommand):
                start_pos = len(output) - command.offset
                i = 0
                while i < command.length:
                    output += output[start_pos + i]
                    i += 1
        return output
