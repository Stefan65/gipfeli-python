#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Methods concerning string matching.
"""

from gipfeli.common.utils import UnalignedDataAccess


class Matcher:
    """
    Collection of string matchers for one or multiple blocks.
    """

    @staticmethod
    def find_match_length(data, s1_start, s2_start, s2_limit):
        """
        Return the largest :code:`n` such that :code:`s1[0, n-1] == s2[0, n-1]`
        and :code:`n <= (s2_limit - s2)`.

        :param data: The data to work on. This is used for the first and the
                     second string by applying the corresponding start offset.
        :type data: str

        :param s1_start: The offset to use for the first string at the
                         beginning.
        :type s1_start: int

        :param s2_start: The offset to use for the second string at the
                         beginning.
        :type s2_start: int

        :param s2_limit: The maximum index allowed inside the second string.
        :type s2_limit: int

        :return: The length of the match.
        :rtype: int
        """
        matched = 0

        s2_position = s2_start

        # We loop over the data 32 bits at once until we find a block of 32
        # bits which does not match.
        while (s2_position <= (s2_limit - 4)) and (
            UnalignedDataAccess.unaligned_load_32(data[s2_position:])
            == UnalignedDataAccess.unaligned_load_32(data[s1_start + matched :])
        ):
            s2_position += 4
            matched += 4

        # We now search for the first non-matching bit and use this to get the
        # total match length.
        while (s2_position < s2_limit) and (
            data[s1_start + matched] == data[s2_position]
        ):
            s2_position += 1
            matched += 1

        return matched

    @staticmethod
    def multi_block_find_match_length(
        data, s1_start, s1_limit, s2_base, s2_start, s2_limit
    ):
        """
        Return the largest match of :code:`n` bytes starting at the first
        string and the second string.

        We assume that both strings are in two separate blocks next to each
        other. That means that once we hit the end of the first string, we
        continue matching from the start of the second string.

        :param data: The data to work on. This is used for the first and the
                     second string by applying the corresponding start offset.
        :type data: str

        :param s1_start: The offset to use for the first string at the
                         beginning.
        :type s1_start: int

        :param s1_limit: The maximum index allowed inside the first string.
        :type s1_limit: int

        :param s2_base: The base index to use for the second string. This is
                        used for the border case.
        :type s2_base: int

        :param s2_start: The offset to use for the second string at the
                         beginning.
        :type s2_start: int

        :param s2_limit: The maximum index allowed inside the second string.
        :type s2_limit: int

        :return: The length of the match.
        :rtype: int
        """
        s2_position = s2_start
        s1_position = s1_start

        # We loop over the data 32 bits at once until we find a block of 32
        # bits which does not match.
        while (
            (s2_position <= (s2_limit - 4))
            and (s1_position <= (s1_limit - 4))
            and (
                UnalignedDataAccess.unaligned_load_32(data[s2_position:])
                == UnalignedDataAccess.unaligned_load_32(data[s1_position:])
            )
        ):
            # Handle the case at the border of both strings where they overlap.
            if s1_position >= s1_limit:
                s1_position = s2_base + (s1_position - s1_limit)
                s1_limit = s2_limit
            s2_position += 4
            s1_position += 4

        # We now search for the first non-matching bit and use this to get the
        # total match length.
        while (
            (s2_position < s2_limit)
            and (s1_position < s1_limit)
            and (data[s1_position] == data[s2_position])
        ):
            s1_position += 1
            s2_position += 1

        return s2_position - s2_start
