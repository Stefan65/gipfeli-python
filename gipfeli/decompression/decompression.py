#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The main decompression routines.
"""

from gipfeli.common.bits import BitReader
from gipfeli.common.utils import UnalignedDataAccess, IncrementalCopy, Math
from gipfeli.compression.lz77 import LiteralCommand, CopyCommand


class Decompressor:
    """
    The general class for performing decompression of data compressed with the
    Gipfeli algorithm.
    """

    COPY_LENGTH_BITS = [0, 0, 2, 2, 2, 3, 3, 6]
    """
    The number of length bits needed for each version of the copy command. This
    list gets indexed by the decimal representation of the used command prefix.

    These values are given in table 3 of the paper and available as
    :attr:`length_length` inside the :code:`decompress.cc` file of the
    reference implementation.

    :type: :class:`list` [:class:`int`]
    """

    COPY_OFFSET_BITS = [0, 0, 10, 13, 16, 10, 16, 16]
    """
    The number of offset/distance bits needed for each version of the copy
    command. This list gets indexed by the decimal representation of the used
    command prefix.

    These values are given in table 3 of the paper and available as
    :attr:`offset_length` inside the :code:`decompress.cc` file of the
    reference implementation.

    :type: :class:`list` [:class:`int`]
    """

    COPY_LENGTH_CHANGE = [0, 0, 4, 4, 4, 8, 8, 4]
    """
    The number to subtract from the length needed for each version of the copy
    command. This list gets indexed by the decimal representation of the used
    command prefix.

    These values are given in table 3 of the paper (the start values inside the
    length column) and available as :attr:`length_change` inside the
    :code:`decompress.cc` file of the reference implementation.

    :type: :class:`list` [:class:`int`]
    """

    def uncompress(self, compressed):
        """
        Uncompress the given input data compressed by the Gipfeli compression
        algorithm.

        :param compressed: The compressed data to uncompress.
        :type compressed: str

        :return: The uncompressed data.
        :rtype: str

        :raises ValueError: The input is invalid.
        """
        compressed_length = len(compressed)
        uncompressed_length = self.get_uncompressed_length(compressed)
        uncompressed = ""

        # Start with the index after the encoded length. This length value
        # uses 1 byte to encode the number of bytes used for the length and
        # then the specified number of bytes for the length value itself.
        index = 1 + ord(compressed[0])

        # Handle each data block.
        while index < compressed_length:
            if (compressed_length - index) < 10:
                # We need 2 bytes for the commands size and at least 8 bytes
                # for the first commands.
                raise ValueError("Not enough bytes left to decode the " "commands.")

            # Extract the commands.
            commands_size = UnalignedDataAccess.unaligned_load_16(
                compressed[index:], "int"
            )
            index += 2
            index, commands = self.decompress_commands(compressed, index, commands_size)

            if (index + 4) > compressed_length:
                # Not enough bytes for the specification of the emptiness of
                # the bitmask segments.
                raise ValueError("Not enough bytes left to decode the " "bitmask.")
            # Get the first 4 bytes from the bitmask.
            bitmask_upper4 = 0
            for _ in range(4):
                # Get the current value.
                value = ord(compressed[index])
                index += 1
                # Each value uses 8 bit, so make room for it.
                bitmask_upper4 <<= 8
                # Add the value itself.
                bitmask_upper4 |= value

            if bitmask_upper4 == 0:
                # No entropy coding is used.
                uncompressed, index = self._uncompress_frame_basic(
                    commands,
                    compressed,
                    compressed_length,
                    uncompressed,
                    uncompressed_length,
                    index,
                )
            else:
                # Entropy coding is used.
                uncompressed, index = self._uncompress_frame_entropy(
                    commands,
                    compressed,
                    compressed_length,
                    uncompressed,
                    uncompressed_length,
                    index,
                    bitmask_upper4,
                )

        if index != compressed_length:
            raise ValueError("The index did not see all the input.")

        return uncompressed

    def _uncompress_frame_basic(
        self,
        commands,
        compressed,
        compressed_length,
        uncompressed,
        uncompressed_length,
        index,
    ):
        """
        Uncompress the content of the current frame in the case that no entropy
        coding has been used.

        This method has been externalized from the general uncompress method to
        keep a better overview.

        :param commands: The commands of the LZ77 algorithm.
        :type commands: list[Lz77Command]

        :param compressed: The compressed string.
        :type compressed: str

        :param compressed_length: The length of the compressed string.
        :type compressed_length: int

        :param uncompressed: The current decompression output.
        :type uncompressed: str

        :param uncompressed_length: The length of the final uncompressed
                                    string.
        :type uncompressed_length: int

        :param index: The current index inside the compressed string.
        :type index: int

        :return: The uncompressed string with the current frame added and the
                 new index value.
        :rtype: tuple[str, int]

        :raises ValueError: The input is invalid.
        """
        for command in commands:
            # Handle each of the commands.
            if isinstance(command, LiteralCommand):
                # Handle simple literal commands where we just have to
                # copy some data from the compressed string to the
                # uncompressed one.
                if (len(uncompressed) + command.length > uncompressed_length) or (
                    index + command.length > compressed_length
                ):
                    raise ValueError(
                        "Not enough bits left to decode " "the literal command."
                    )
                uncompressed += compressed[index : index + command.length]
                index += command.length
            elif isinstance(command, CopyCommand):
                # Handle copy commands.
                length = command.length
                offset = command.offset
                space_left = uncompressed_length - len(uncompressed)

                # -1 prevents infinite loop if offset is 0.
                if len(uncompressed) <= (offset - 1):
                    raise ValueError("Infinite loop detected.")

                # Calculate the start index of the copy operation as this is
                # needed in all cases.
                index_start = len(uncompressed) - offset
                if (length <= 16) and (offset >= 8) and (space_left >= 16):
                    # Fast path, used for the majority (70-80 %) of dynamic
                    # invocations. We do not have to consider overlaps.
                    uncompressed += uncompressed[index_start : index_start + length]
                elif space_left >= (
                    length + IncrementalCopy.MAX_INCREMENT_COPY_OVERFLOW
                ):
                    # We have to consider overlaps and will not fall into the
                    # worst case scenario for fast path copies.
                    uncompressed += IncrementalCopy.copy_fast_path(
                        uncompressed, index_start, length
                    )
                else:
                    # We have to consider overlaps, but the worst case scenario
                    # for the fast version might be possible.
                    if space_left < length:
                        raise ValueError("Not enough space left.")
                    uncompressed += IncrementalCopy.copy(
                        uncompressed, index_start, length
                    )

        return uncompressed, index

    def _uncompress_frame_entropy(
        self,
        commands,
        compressed,
        compressed_length,
        uncompressed,
        uncompressed_length,
        index,
        mask_upper4,
    ):
        """
        Uncompress the content of the current frame in the case that entropy
        coding has been used.

        This method has been externalized from the general uncompress method to
        keep a better overview.

        :param commands: The commands of the LZ77 algorithm.
        :type commands: list[Lz77Command]

        :param compressed: The compressed string.
        :type compressed: str

        :param compressed_length: The length of the compressed string.
        :type compressed_length: int

        :param uncompressed: The current decompression output.
        :type uncompressed: str

        :param uncompressed_length: The length of the final uncompressed
                                    string.
        :type uncompressed_length: int

        :param index: The current index inside the compressed string.
        :type index: int

        :param mask_upper4: The upper 4 bits of the mask.
        :type mask_upper4: int

        :return: The uncompressed string with the current frame added and the
                 new index value.
        :rtype: tuple[str, int]

        :raises ValueError: The input is invalid.
        """
        if (index + Math.count_ones(mask_upper4) + 12) > compressed_length:
            # The index already contains the first 4 bytes, for each of the
            # 32 segments we use a byte if it is not empty. The 12 bytes are
            # needed for the second part of the mask.
            raise ValueError("Not enough bytes to decode the bitmask.")
        # Build the conversion tables.
        index, table_6bit, table_8bit = self.decompress_mask(
            mask_upper4, compressed, index
        )
        if index >= compressed_length:
            raise ValueError("Not enough input data for the content.")
        bit_reader = BitReader(compressed, index, compressed_length)
        if (compressed_length - index) < 8:
            raise ValueError("Not enough input data for the content.")

        for command in commands:
            if isinstance(command, LiteralCommand):
                # Handle literal commands.
                if (len(uncompressed) + command.length) > uncompressed_length:
                    raise ValueError("Output bigger than expected.")

                for _ in range(command.length):
                    # Iterate over all characters.
                    # All literals are encoded with at least 6 bit.
                    value = bit_reader.read(6)
                    if value < 32:
                        # We have one of the first 32 symbols using 6 bit.
                        uncompressed += chr(table_6bit[value])
                    elif value >= 48:
                        # We have one of the last 160 symbols using 10 bit.
                        value = ((value - 48) << 4) + bit_reader.read(4)
                        uncompressed += chr(value)
                    else:
                        # We have one of the inner 64 symbols using 8 bit.
                        value = ((value - 32) << 2) + bit_reader.read(2)
                        uncompressed += chr(table_8bit[value])
            elif isinstance(command, CopyCommand):
                # Handle copy commands.
                length = command.length
                offset = command.offset
                space_left = uncompressed_length - len(uncompressed)

                # -1 prevents infinite loop if offset is 0.
                if len(uncompressed) <= (offset - 1):
                    raise ValueError("Infinite loop detected.")

                # Calculate the start index of the copy operation as this is
                # needed in all cases.
                index_start = len(uncompressed) - offset
                if (length <= 16) and (offset >= 8) and (space_left >= 16):
                    # Fast path, used for the majority (70-80 %) of dynamic
                    # invocations. We do not have to consider overlaps.
                    uncompressed += uncompressed[index_start : index_start + length]
                elif space_left >= (
                    length + IncrementalCopy.MAX_INCREMENT_COPY_OVERFLOW
                ):
                    # We have to consider overlaps and will not fall into the
                    # worst case scenario for fast path copies.
                    uncompressed += IncrementalCopy.copy_fast_path(
                        uncompressed, index_start, length
                    )
                else:
                    # We have to consider overlaps, but the worst case scenario
                    # for the fast version might be possible.
                    if space_left < length:
                        raise ValueError("Not enough space left.")
                    uncompressed += IncrementalCopy.copy(
                        uncompressed, index_start, length
                    )

        index = bit_reader.stop()
        if index > compressed_length:
            raise ValueError("Index exceeded the input data.")

        return uncompressed, index

    @staticmethod
    def get_uncompressed_length(compressed):
        """
        Get the length of the uncompressed data.

        :param compressed: The string to determine the length for.
        :type compressed: str

        :return: The uncompressed length.
        :rtype: int

        :raises ValueError: A parsing error occurred.
        """
        if not compressed:
            raise ValueError("No data to handle.")

        bytes_used = ord(compressed[0])
        if bytes_used > 4:
            raise ValueError("Too much length bytes specified.")
        if len(compressed) < (1 + bytes_used):
            raise ValueError("Not enough input data to read the length value.")

        uncompressed_length = 0
        for i in range(bytes_used, 0, -1):
            uncompressed_length <<= 8
            uncompressed_length |= ord(compressed[i])

        return uncompressed_length

    def decompress_mask(self, upper, data, start_position):
        """
        Read the bitmask from the given data which contains information about
        the entropy code. The conversion tables for 6 bit and 8 bit values are
        built, which assign to each compressed symbol of length 6 or 8 bits the
        original symbol. They will be used later to convert the literals.

        :param upper: The first 4 bytes of the bitmask specifying which of the
                      32 segments are empty.
        :type upper: int

        :param data: The input to get the bitmask from.
        :type data: str

        :param start_position: The position to start with inside the data.
        :type start_position: int

        :return: The new index inside the input, the conversion table for the
                 symbols with 6 bit and the conversion table for the symbols
                 with 8 bit.
        :rtype: tuple[int, list[int], list[int]]

        :raises ValueError: The input is invalid.
        """
        table_6bit = [0] * 32
        table_8bit = [0] * 64
        index = start_position

        # First phase: Read 96 symbols that will have 6 or 8 bit long symbols.
        to_be_converted = [0] * 96
        count = 0

        # This value is used to determine if the current segment is not empty.
        # We start with the bit on the left.
        and_value = 1 << 31
        for i in range(32):
            # Handle all the segments.
            if (and_value & upper) != 0:
                # The current segment is not empty.
                for j in range(8):
                    # Handle each of the symbols of the segment.
                    if (ord(data[index]) & (1 << (7 - j))) != 0:
                        # The current symbol belongs to one of the first two
                        # groups. The value to convert gets the corresponding
                        # index inside the non-empty segments.
                        if count >= 96:
                            raise ValueError(
                                "Found too much symbols for the"
                                "first two mask groups."
                            )
                        to_be_converted[count] = 8 * i + j
                        count += 1
                index += 1
            # Look at the bit to the right of the current one in the next step.
            and_value >>= 1

        if count != 96:
            raise ValueError("Wrong number of symbols for the first two " "groups.")

        # Second phase: Split 96 symbols into ones with 6 bit and ones with 8
        # bit long codes and assign the symbol to every code.
        count_6bit = 0
        count_8bit = 0

        i_end = (count + 7) // 8
        for i in range(i_end):
            for j in range(8):
                if (8 * i + j) < count:
                    # The current value belongs to one of the groups.
                    if (ord(data[index]) & (1 << (7 - j))) != 0:
                        # The current value belongs to the first group.
                        if count_6bit >= 32:
                            raise ValueError("Too much symbols for the first " "group.")
                        table_6bit[count_6bit] = to_be_converted[8 * i + j]
                        count_6bit += 1
                    else:
                        # The current value belongs to the second group.
                        if count_8bit >= 64:
                            raise ValueError("Too much symbols for the second" "group.")
                        table_8bit[count_8bit] = to_be_converted[8 * i + j]
                        count_8bit += 1
            index += 1

        if (count_6bit != 32) or (count_8bit != 64):
            raise ValueError("Wrong number of symbols inside the first two " "groups.")

        return index, table_6bit, table_8bit

    def decompress_commands(self, data, start_position, number_of_commands):
        """
        Decompress the commands.

        :param data: The input to get the data from.
        :type data: str

        :type start_position: The index to start at.
        :type start_position: int

        :param number_of_commands: The number of commands available from the
                                   compressed data.
        :type number_of_commands: int

        :return: The current index and the commands for the LZ77 algorithm.
        :rtype: tuple[int, list[Lz77Command]]

        :raises ValueError: The input is invalid.
        """
        # Prepare the variables.
        input_end = len(data) - 1
        bit_reader = BitReader(data, start_position, input_end)
        commands = []

        # Decode all the commands.
        for _ in range(number_of_commands):
            # A maximum of 3 bits holds the prefix for the command.
            value = bit_reader.read(3)
            if value < 2:
                # We found a literal command. Read the next 5 bits to determine
                # the bit length of the encoded length.
                value = (value << 5) + bit_reader.read(5)
                if value < 53:
                    # We have 8 bits, starting with 00. For this reason, save
                    # the 6 length bits (as the 00 at the beginning do no
                    # harm, we do not have to remove them).
                    command = LiteralCommand(value + 1)
                else:
                    # We have 8 bits specifying a bit length from 6 to 16.
                    # Read the missing bits and save the length value.
                    command = LiteralCommand(bit_reader.read(value - 47) + 1)
            else:
                # We found a copy command. Read the needed number of bits to
                # get the corresponding length and offset values.
                length = (
                    bit_reader.read(self.COPY_LENGTH_BITS[value])
                    + self.COPY_LENGTH_CHANGE[value]
                )
                offset = bit_reader.read(self.COPY_OFFSET_BITS[value]) + 1
                command = CopyCommand(offset, length)
            # Add the decoded command to the others we have already encoded
            # before.
            commands.append(command)

        return bit_reader.stop(), commands
