#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This is the public interface of Gipfeli which should be sufficient for the
most use cases.
"""

from gipfeli.compression.compression import Compressor
from gipfeli.decompression.decompression import Decompressor


class Gipfeli:
    """
    The main class suitable for the basic execution of the Gipfeli algorithm.
    It acts as an interface to the real implementations inside the submodules.

    Example usage:

    .. code-block:: python

       data = "This is my string I want to compress."
       compressed = Gipfeli.compress(data)
       uncompressed = Gipfeli.uncompress(compressed)
       assert(data == uncompressed)
    """

    def __init__(self):
        pass

    @staticmethod
    def compress(uncompressed):
        """
        Compress the given string using the Gipfeli algorithm.

        :param uncompressed: The data to compress.
        :type uncompressed: str

        :return: The compression result.
        :rtype: str
        """
        return Compressor().compress(uncompressed)

    @staticmethod
    def compress_file(input_file, output_file=None):
        """
        Compress the content of the file using the Gipfeli algorithm. If an
        output file is specified, the compressed data gets written to it.

        :param input_file: The path to the file to get the uncompressed data
                           from.
        :type input_file: str

        :param output_file: The path to the file to write the compressed data
                            to. If this is empty or :class:`None`, no data gets
                            written.
        :type output_file: str or None

        :return: The compressed data.
        :rtype: str
        """
        with open(input_file, mode="rb") as infile:
            uncompressed_bytes = infile.read()
        uncompressed = "".join([chr(x) for x in uncompressed_bytes])
        compressed = Gipfeli.compress(uncompressed)
        if output_file:
            # We do not want to specify an encoding for the compressed string.
            # For this reason we extract the ASCII values into a list and feed
            # this data into a byte array to be written to the file.
            compressed_int_list = [ord(x) for x in list(compressed)]
            with open(output_file, mode="wb") as outfile:
                outfile.write(bytearray(compressed_int_list))
        return compressed

    @staticmethod
    def uncompress(compressed):
        """
        Decompress the given string which has been compressed by the Gipfeli
        algorithm.

        :param compressed: The string to decompress which has been compressed
                           using the Gipfeli algorithm beforehand.
        :type compressed: str

        :return: The original data.
        :rtype: str
        """
        return Decompressor().uncompress(compressed)

    @staticmethod
    def uncompress_file(input_file, output_file=None):
        """
        Uncompress the content of the file created by the Gipfeli algorithm. If
        an output file is specified, the uncompressed data gets written to it.

        :param input_file: The path to the file go get the compressed data
                           from.
        :type input_file: str

        :param output_file: The path to the file to write the uncompressed data
                            to. If this is empty or :class:`None`, no data gets
                            written.
        :type output_file: str or None

        :return: The uncompressed data.
        :rtype: str
        """
        with open(input_file, mode="rb") as infile:
            compressed_bytes = infile.read()
        compressed = "".join([chr(x) for x in compressed_bytes])
        uncompressed = Gipfeli.uncompress(compressed)
        if output_file:
            # We do not want to specify an encoding for the uncompressed string.
            # For this reason we extract the ASCII values into a list and feed
            # this data into a byte array to be written to the file.
            uncompressed_int_list = [ord(x) for x in list(uncompressed)]
            with open(output_file, mode="wb") as outfile:
                outfile.write(bytearray(uncompressed_int_list))
        return uncompressed

    @staticmethod
    def get_max_compressed_length(number_of_source_bytes):
        """
        Get the maximal size of the compressed representation of the input data
        which is the given number of bytes in length.

        :param number_of_source_bytes: The length of the input data (number of
                                       bytes).
        :type number_of_source_bytes: int

        :return: The maximal size of the compressed representation.
        :rtype: int
        """
        return Compressor.get_max_compressed_length(number_of_source_bytes)
