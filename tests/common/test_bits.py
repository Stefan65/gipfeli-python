#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`gipfeli.common.bits` module.
"""

import unittest

from gipfeli.common.bits import BitReader, BitWriter


class BitReaderTests(unittest.TestCase):
    """
    Tests for the bit reader.
    """

    def test_read_without_jump(self):
        """
        Test the read operation without a jump to the next value.
        """
        # The input data is (with taking the unaligned load into account)
        # "0000000000000000000000000000000000000000000000000000000000000001"
        # for the first 8 bytes. The next 8 bytes will never be read.
        bit_reader = BitReader("\x01" + "\x00" * 7 + "\x03" * 8, 0, 8)
        self.assertEqual(bit_reader.read(2), int("00", 2))
        self.assertEqual(bit_reader.read(60), int("0" * 60, 2))
        self.assertEqual(bit_reader.read(2), int("01", 2))
        self.assertEqual(bit_reader.stop(), 8)

    def test_read_with_jump(self):
        """
        Test the read operation with a jump to the next value.
        """
        # The input data is (with taking the unaligned load into account)
        # "0000000000000000000000000000000000000000000000000000000000000001"
        # for the first 8 bytes and
        # "0000000100000000000000010000000000000001000000000000000100000000"
        bit_reader = BitReader("\x01" + "\x00" * 7 + "\x00\x01" * 4, 0, 16)
        self.assertEqual(bit_reader.read(40), int("0" * 40, 2))
        self.assertEqual(
            bit_reader.read(40), int("0" * 23 + "1" + "0" * 7 + "1" + "0" * 8, 2)
        )
        self.assertEqual(bit_reader.stop(), 16)

    def test_read_too_many_bits_requested(self):
        """
        Test the read operation when requesting too many bits.
        """
        bit_reader = BitReader("\x00" * 8, 0, 8)
        self.assertRaises(ValueError, bit_reader.read, 70)


class BitWriterTests(unittest.TestCase):
    """
    Tests for the bit writer.
    """

    def test_without_jump(self):
        """
        Test without starting a new buffer in-between.
        """
        bit_writer = BitWriter()
        bit_writer.write(8, 64)
        bit_writer.flush()
        self.assertEqual(bit_writer.output, "\x00" * 7 + chr(64))

    def test_not_full_bytes(self):
        """
        Test without writing full bytes at once.
        """
        bit_writer = BitWriter()
        bit_writer.write(6, 4)
        bit_writer.write(2, 3)
        bit_writer.flush()
        self.assertEqual(bit_writer.output, "\x00" * 7 + chr(int("000100" + "11", 2)))

    def test_with_jump(self):
        """
        Test with clearing the buffer in-between.
        """
        bit_writer = BitWriter()
        bit_writer.write(70, 0)
        bit_writer.write(10, 2)
        bit_writer.flush()
        self.assertEqual(
            bit_writer.output,
            "\x00" * 8 + "\x00" * 6 + chr(int("0" * 10 + "0000000010", 2)) + "\x00",
        )
