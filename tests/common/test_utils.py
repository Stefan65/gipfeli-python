#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`gipfeli.common.utils` module.
"""

import unittest

from gipfeli.common.utils import IncrementalCopy, Math, UnalignedDataAccess


class IncrementalCopyTests(unittest.TestCase):
    """
    Tests for incremental copy operations.
    """

    def _test_equality(self, source, start, length, expected):
        """
        Perform the test itself. This checks both methods with the same input.

        :param source: The source string.
        :type source: str

        :param start: The index to start at.
        :type start: int

        :param length: The number of characters to copy.
        :type length: int

        :param expected: The expected output.
        :type expected: str
        """
        self.assertEqual(
            IncrementalCopy.copy_fast_path(source, start, length), expected
        )
        self.assertEqual(IncrementalCopy.copy(source, start, length), expected)

    def test_single_character_input(self):
        """
        Simple test case with a single character as source.
        """
        self._test_equality("-", 0, 10, "-" * 10)

    def test_short_recurring_pattern(self):
        """
        Test using a short pattern with less than 4 characters as source.
        """
        self._test_equality("+-*/", 0, 10, "+-*/+-*/+-")

    def test_no_overlap(self):
        """
        Test without having to consider an overlap.
        """
        self._test_equality("0123456789", 2, 5, "23456")

    def test_multiple_of_eight(self):
        """
        Test with the requested length being a multiple of eight.
        """
        self._test_equality("-", 0, 32, "-" * 32)

    def test_multiple_ranges(self):
        """
        Test with different lengths in an interval.
        """
        for length in range(10):
            self._test_equality("-", 0, length, "-" * length)

    def test_maximum_length(self):
        """
        Test with the maximum length allowed for a copy command inside the
        Gipfeli algorithm.
        """
        self._test_equality("-", 0, 67, "-" * 67)

    def test_documentation_example(self):
        """
        Test with the example from the documentation of the
        :func:`~gipfeli.common.utils.IncrementalCopy.copy` method.
        """
        self._test_equality("ab", 0, 20, "ab" * 10)


class MathTests(unittest.TestCase):
    """
    Tests for the math-related methods.
    """

    def test_count_ones_zero(self):
        """
        Count the ones inside the binary representation of zero.
        """
        self.assertEqual(Math.count_ones(0), 0)

    def test_count_ones_one(self):
        """
        Count the ones inside the binary representation of one.
        """
        self.assertEqual(Math.count_ones(1), 1)

    def test_count_ones_bigger(self):
        """
        Count the ones inside the binary representation of a bigger value.
        """
        self.assertEqual(Math.count_ones(int("10" * 10, 2)), 10)

    def test_is_divisible_zero(self):
        """
        Test whether zero is divisible.
        """
        self.assertTrue(Math.is_divisible(0, 2))

    def test_is_divisible_same(self):
        """
        Test whether one number is divisible by itself.
        """
        self.assertTrue(Math.is_divisible(2, 2))

    def test_is_divisible_bigger(self):
        """
        Test whether a bigger number is divisible.
        """
        self.assertTrue(Math.is_divisible(27**7, 27))

    def test_is_divisible_prime(self):
        """
        Test whether a prime number is divisible.
        """
        self.assertFalse(Math.is_divisible(97, 3))

    def test_is_power_one(self):
        """
        Test whether one can be written as a power.
        """
        self.assertTrue(Math.is_power(1, 2))

    def test_is_power_same(self):
        """
        Test whether a number can be written as a power of itself.
        """
        self.assertTrue(Math.is_power(2, 2))

    def test_is_power_bigger(self):
        """
        Test whether a bigger number can be written as a power.
        """
        self.assertTrue(Math.is_power(42**23, 42))

    def test_log2_floor_two(self):
        """
        Determine the binary logarithm of two.
        """
        self.assertEqual(Math.log2_floor(2), 1)

    def test_log2_floor_one(self):
        """
        Determine the binary logarithm of one.
        """
        self.assertEqual(Math.log2_floor(1), 0)

    def test_log2_floor_bigger(self):
        """
        Determine the binary logarithm of a bigger number.
        """
        self.assertEqual(Math.log2_floor(2**43 - 1), 42)


class UnalignedDataAccessTests(unittest.TestCase):
    """
    Tests for the load and store methods.
    """

    def test_load_normal(self):
        """
        Test a load with an input of at least the needed length.
        """
        self.assertEqual(
            UnalignedDataAccess.unaligned_load(
                "abcdef", number_of_bytes=4, return_type="str"
            ),
            "abcd",
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_load(
                "abcdef", number_of_bits=4 * 8, return_type="str"
            ),
            "abcd",
        )

    def test_load_too_short_input(self):
        """
        Test a load with a too short input.
        """
        self.assertEqual(
            UnalignedDataAccess.unaligned_load(
                "ab", number_of_bytes=4, return_type="str"
            ),
            "ab",
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_load(
                "ab", number_of_bits=4 * 8, return_type="str"
            ),
            "ab",
        )

    def test_store_normal(self):
        """
        Test a store with an input of at least the needed length.
        """
        self.assertEqual(
            UnalignedDataAccess.unaligned_store(
                "abcdef", number_of_bytes=4, return_type="str"
            ),
            "abcd",
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_store(
                "abcdef", number_of_bits=4 * 8, return_type="str"
            ),
            "abcd",
        )

    def test_store_too_short_input(self):
        """
        Test a store with a too short input.
        """
        self.assertEqual(
            UnalignedDataAccess.unaligned_store(
                "ab", number_of_bytes=4, return_type="str"
            ),
            "ab",
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_store(
                "ab", number_of_bits=4 * 8, return_type="str"
            ),
            "ab",
        )

    def test_load_store_equals_string_normal(self):
        """
        Check the storing a loaded string with a string as intermediate format
        and an input of at least the needed length works.
        """
        self.assertEqual(
            UnalignedDataAccess.unaligned_store(
                UnalignedDataAccess.unaligned_load(
                    "abcdef", number_of_bytes=4, return_type="str"
                ),
                number_of_bytes=4,
                return_type="str",
            ),
            "abcd",
        )

    def test_load_store_equals_string_too_short(self):
        """
        Check the storing a loaded string with a string as intermediate format
        and a too short input works.
        """
        self.assertEqual(
            UnalignedDataAccess.unaligned_store(
                UnalignedDataAccess.unaligned_load(
                    "ab", number_of_bytes=4, return_type="str"
                ),
                number_of_bytes=4,
                return_type="str",
            ),
            "ab",
        )

    def test_load_store_equals_integer_normal(self):
        """
        Check the storing a loaded string with an integer as intermediate
        format and an input of at least the needed length works.
        """
        self.assertEqual(
            UnalignedDataAccess.unaligned_store(
                UnalignedDataAccess.unaligned_load(
                    "abcdef", number_of_bytes=4, return_type="int"
                ),
                number_of_bytes=4,
                return_type="str",
            ),
            "abcd",
        )

    def test_load_store_equals_integer_too_short(self):
        """
        Check the storing a loaded string with an integer as intermediate
        format and a too short input works.
        """
        self.assertEqual(
            UnalignedDataAccess.unaligned_store(
                UnalignedDataAccess.unaligned_load(
                    "ab", number_of_bytes=4, return_type="int"
                ),
                number_of_bytes=4,
                return_type="str",
            ),
            "ab\x00\x00",
        )

    def test_load_16(self):
        """
        Check that the wrapper to load 16 bits works as expected.
        """
        data = "abcdef"
        self.assertEqual(
            UnalignedDataAccess.unaligned_load_16(data, "str"),
            UnalignedDataAccess.unaligned_load(
                data, number_of_bytes=2, return_type="str"
            ),
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_load_16(data, "str"),
            UnalignedDataAccess.unaligned_load(
                data, number_of_bits=2 * 8, return_type="str"
            ),
        )

    def test_load_32(self):
        """
        Check that the wrapper to load 32 bits works as expected.
        """
        data = "abcdef"
        self.assertEqual(
            UnalignedDataAccess.unaligned_load_32(data, "str"),
            UnalignedDataAccess.unaligned_load(
                data, number_of_bytes=4, return_type="str"
            ),
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_load_32(data, "str"),
            UnalignedDataAccess.unaligned_load(
                data, number_of_bits=4 * 8, return_type="str"
            ),
        )

    def test_load_64(self):
        """
        Check that the wrapper to load 64 bits works as expected.
        """
        data = "abcdef"
        self.assertEqual(
            UnalignedDataAccess.unaligned_load_64(data, "str"),
            UnalignedDataAccess.unaligned_load(
                data, number_of_bytes=8, return_type="str"
            ),
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_load_64(data, "str"),
            UnalignedDataAccess.unaligned_load(
                data, number_of_bits=8 * 8, return_type="str"
            ),
        )

    def test_store_16(self):
        """
        Check that the wrapper to store 16 bits works as expected.
        """
        data = "abcdef"
        self.assertEqual(
            UnalignedDataAccess.unaligned_store_16(data, "str"),
            UnalignedDataAccess.unaligned_store(
                data, number_of_bytes=2, return_type="str"
            ),
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_store_16(data, "str"),
            UnalignedDataAccess.unaligned_store(
                data, number_of_bits=2 * 8, return_type="str"
            ),
        )

    def test_store_32(self):
        """
        Check that the wrapper to store 32 bits works as expected.
        """
        data = "abcdef"
        self.assertEqual(
            UnalignedDataAccess.unaligned_store_32(data, "str"),
            UnalignedDataAccess.unaligned_store(
                data, number_of_bytes=4, return_type="str"
            ),
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_store_32(data, "str"),
            UnalignedDataAccess.unaligned_store(
                data, number_of_bits=4 * 8, return_type="str"
            ),
        )

    def test_store_64(self):
        """
        Check that the wrapper to store 64 bits works as expected.
        """
        data = "abcdef"
        self.assertEqual(
            UnalignedDataAccess.unaligned_store_64(data, "str"),
            UnalignedDataAccess.unaligned_store(
                data, number_of_bytes=8, return_type="str"
            ),
        )
        self.assertEqual(
            UnalignedDataAccess.unaligned_store_64(data, "str"),
            UnalignedDataAccess.unaligned_store(
                data, number_of_bits=8 * 8, return_type="str"
            ),
        )
