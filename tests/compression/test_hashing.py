#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`gipfeli.compression.hashing` module.
"""

import unittest

from gipfeli.compression.hashing import HashTable, UnhashedTable


class ValueRangeTests(unittest.TestCase):
    """
    Tests to ensure the correct value range for the hash table.
    """

    def _test_equality(self, key, value, expected_value):
        """
        Test whether setting the value for the key leads to the expected value.

        This resets the hash table before every call.

        :param key: The key to use.
        :type key: str

        :param value: The value to set for the key.
        :type value: int

        :param expected_value: The value which should be returned when
                               accessing the key.
        :type expected_value: int
        """
        hash_table = HashTable()
        unhashed_table = UnhashedTable()

        hash_table.set(key, value)
        unhashed_table.set(key, value)

        self.assertEqual(hash_table.get(key), expected_value)
        self.assertEqual(unhashed_table.get(key), expected_value)

    def test_normal(self):
        """
        Test with normal values which do not need to be modified.
        """
        self._test_equality("test", 10, 10)
        self._test_equality("test", 60000, 60000)

    def test_upper_border(self):
        """
        Test with values within a distance of two around the upper limit.
        """
        self._test_equality("test", 2**16 - 2, 2**16 - 2)
        self._test_equality("test", 2**16 - 1, 2**16 - 1)
        self._test_equality("test", 2**16, 0)
        self._test_equality("test", 2**16 + 1, 1)
        self._test_equality("test", 2**16 + 2, 2)

    def test_bigger(self):
        """
        Test with bigger values exceeding the upper limit.
        :return:
        """
        self._test_equality("test", 2**17, 2**17 - 2**16)
