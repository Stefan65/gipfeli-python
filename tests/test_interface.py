#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`gipfeli.interface` module.
"""

import os
import tempfile
import unittest

import lipsum

from gipfeli.interface import Gipfeli


class StringBasedTests(unittest.TestCase):
    """
    Tests of the string-based routines.
    """

    def _test_equality(self, uncompressed):
        """
        Check if the uncompressed version of the compressed output matches the
        original (uncompressed) data.

        :param uncompressed: The data to compress.
        :type uncompressed: str
        """
        compressed = Gipfeli.compress(uncompressed)
        self.assertEqual(uncompressed, Gipfeli.uncompress(compressed))

    def test_empty(self):
        """
        Test with an empty string.
        """
        self._test_equality("")

    def test_short(self):
        """
        Test with a short text.
        """
        self._test_equality("Lorem ipsum")

    def test_sentence(self):
        """
        Test with one random sentence.
        """
        self._test_equality(lipsum.generate_sentences(1))

    def test_paragraph(self):
        """
        Test with one random paragraph.
        """
        self._test_equality(lipsum.generate_paragraphs(1))

    def test_paragraphs(self):
        """
        Test with ten random paragraphs.
        """
        self._test_equality(lipsum.generate_paragraphs(10))

    def test_multiple_fragments(self):
        """
        Test with multiple fragments. This uses a random string with a length
        of at least 2^17 characters by generating 10 random paragraphs at once.
        """
        data = ""
        needed_characters = 2**17
        while len(data) < needed_characters:
            data += lipsum.generate_paragraphs(10)
        self._test_equality(data)


class FileBasedTests(unittest.TestCase):
    """
    Tests of the file-based routines.
    """

    _output_path = os.path.join(tempfile.gettempdir(), "gipfeli")
    """
    The directory to write the output data to.

    :type: str
    """

    def setUp(self):
        # Create the output path.
        if not os.path.exists(self._output_path):
            os.makedirs(self._output_path)

    def _test_equality(self, filename):
        """
        Check if the uncompressed version of the compressed output matches the
        original (uncompressed) data. The input is read from the given
        file.

        :param filename: The input file to read the data from.
        :type filename: str
        """
        # Add the path prefix if the file could not be found.
        input_file = filename
        if not os.path.exists(input_file):
            input_file = "data/{}".format(input_file)
        if not os.path.exists(input_file):
            input_file = "tests/{}".format(input_file)

        # Write to a temporary file.
        compressed_file = os.path.join(self._output_path, "{}.gipfeli".format(filename))
        uncompressed_file = os.path.join(self._output_path, filename)

        # Perform the test itself.
        Gipfeli.compress_file(input_file, compressed_file)
        Gipfeli.uncompress_file(compressed_file, uncompressed_file)

        # Compare the content of the original file and the uncompressed
        # compressed file. We could use the ratio of the
        # difflib.SequenceMatcher class here, but as we have to read the files
        # anyways, a direct equality assert on the strings is much easier to
        # understand.
        with open(input_file, mode="rb") as infile:
            original_data = infile.readlines()
        with open(uncompressed_file, mode="rb") as infile:
            uncompressed_data = infile.readlines()
        self.assertEqual(original_data, uncompressed_data)

    def test_xy_short(self):
        """
        Test with a simple short file.
        """
        self._test_equality("xy_short.txt")

    def test_fireworks(self):
        """
        Test with a firework image.
        """
        self._test_equality("fireworks.jpeg")

    def test_cicero(self):
        """
        Test with all books of "De Finibus Bonorum et Malorum" by Marcus
        Tullius Cicero.
        """
        self._test_equality("cicero_deFinibusBonorumEtMalorum.txt")
