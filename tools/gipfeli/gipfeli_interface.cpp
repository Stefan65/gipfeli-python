/**
 * @brief Terminal interface for Gipfeli.
 * @details Terminal interface for the reference implementation of the Gipfeli
 *          algorithm. The parameters are kept as similar to the Python
 *          version as possible.
 * @author Stefan65
 * @version 0.1
 */

// Include standard libraries.
#include <fstream>
#include <iostream>

// Include the option parser.
#include <cxxopts.hpp>

// Include the Gipfeli implementation.
#include <gipfeli.h>
#include <stubs-internal.h>

/**
 * Check if the given string is empty.
 *
 * @param s The string to check.
 * @return Whether the string is empty or not.
 */
bool is_string_empty(const std::string& s) {
    return s[0] == 0;
}

/**
 * Read the given file into the given variable.
 *
 * This method has been taken from the test file of the reference
 * implementation.
 *
 * @param filename The name of the file to be read.
 * @param content The variable to save the content of the file inside.
 */
void read_file(const std::string& filename, std::string* content) {
    std::ifstream ifs(filename.c_str());
    content->assign((std::istreambuf_iterator<char>(ifs)),
                    (std::istreambuf_iterator<char>()));
}

/**
 * Write the given content into the given file.
 *
 * @param filename The name of the file to write to.
 * @param content The data to write to the file.
 */
void write_file(const std::string& filename, const std::string& content) {
    std::ofstream outfile(filename);
    outfile << content;
    outfile.close();
}

/**
 * Compress the given string.
 *
 * @param original The string to compress.
 * @return The compressed string.
 */
std::string compress(const std::string& original) {
    std::string compressed;
    util::compression::Compressor* compressor =
        util::compression::NewGipfeliCompressor();
    const size_t compressed_size = compressor->Compress(original,
                                                        &compressed);
    return compressed;
}

/**
 * Uncompress the given string.
 *
 * @param compressed The string to uncompress.
 * @return The uncompressed string.
 */
std::string uncompress(const std::string& compressed) {
    std::string uncompressed;
    util::compression::Compressor* compressor =
        util::compression::NewGipfeliCompressor();
    const size_t uncompressed_size = compressor->Uncompress(compressed,
                                                            &uncompressed);
    return uncompressed;
}

/**
 * Compress the given file and write the result to a file if requested.
 *
 * @param input_file The file to read the original data from.
 * @param output_file The file to write the compressed data to. Set to an
 *                    empty string if you do not want the result to be written
 *                    to a file.
 * @return The compressed string.
 */
std::string compress_file(const std::string& input_file,
                          const std::string& output_file) {
    std::string original;
    read_file(input_file, &original);
    const std::string compressed = compress(original);

    if (!is_string_empty(output_file)) {
        write_file(output_file, compressed);
    }
    return compressed;
}

/**
 * Uncompress the given file and write the result to a file if requested.
 *
 * @param input_file The file to read the compressed data from.
 * @param output_file The file to write the uncompressed data to. Set to an
 *                    empty string if you do not want the result to be written
 *                    to a file.
 * @return The uncompressed string.
 */
std::string uncompress_file(const std::string& input_file,
                            const std::string& output_file) {
    std::string compressed;
    read_file(input_file, &compressed);
    const std::string uncompressed = uncompress(compressed);

    if (!is_string_empty(output_file)) {
        write_file(output_file, uncompressed);
    }
    return uncompressed;
}

/**
 * The main entry point of the application responsible for parsing the
 * arguments and calling the appropriate methods.
 *
 * @param argc The number of arguments specified.
 * @param argv The specified arguments.
 */
int main(int argc, char* argv[]) {
    try {
        // Instantiate an argument parser.
        cxxopts::Options options(argv[0], " - Gipfeli Terminal Interface "
                                          "(Reference Implementation)");
        options
            .positional_help("[optional args]")
            .show_positional_help();

        // Add the switches.
        options.add_options("Action type")
            ("c,compress", "Compress a file.",
                cxxopts::value<bool>())
            ("d,decompress", "Decompress/uncompress a file.",
                cxxopts::value<bool>());

        // Add the arguments.
        options.add_options("File")
            ("infile", "The name of the input file.",
                cxxopts::value<std::string>())
            ("outfile", "The name of the output file. If no file is "
                "specified, the output gets written to the terminal.",
                cxxopts::value<std::string>()->default_value(""));

        // Add the help.
        options.add_options("Help")
            ("h,help", "Print help.");

        // Start parsing the user input.
        options.parse_positional({"infile", "outfile"});
        auto result = options.parse(argc, argv);

        // Display the help if needed.
        if (result.count("h")) {
            std::cout << options.help({"Action type", "File", "Help"}) 
                << std::endl;
            exit(0);
        }

        // We must have an input file.
        if (!result.count("infile")) {
            std::cout << "An input file has to be specified." << std::endl;
            exit(1);
        }

        // Retrieve the values for better readability.
        const std::string infile = result["infile"].as<std::string>();
        const std::string outfile = result["outfile"].as<std::string>();
        const bool compress = result["compress"].as<bool>();
        const bool uncompress = result["decompress"].as<bool>();

        // We cannot compress and uncompress at the same time.
        if (compress && uncompress) {
            std::cout << "You are not allowed to choose more than one "
                "action." << std::endl;
            exit(1);
        }

        // Choose the right option to execute.
        if (compress || !uncompress) {
            // Compress a file.
            const std::string compressed = compress_file(infile, outfile);
            if (is_string_empty(outfile)) {
                std::cout << compressed << std::endl;
            }
        } else if (uncompress || !compress) {
            // Uncompress a file.
            const std::string uncompressed = uncompress_file(infile, outfile);
            if (is_string_empty(outfile)) {
                std::cout << uncompressed << std::endl;
            }
        } else {
            // We do not know what to do.
            std::cout << "Got an unexpected value and could not decide which"
                " action to perform." << std::endl;
            exit(1);
        }

    } catch (const cxxopts::OptionException& exception) {
        // Another parsing error occurred.
        std::cout << "Error parsing options: " << exception.what()
            << std::endl;
        exit(1);
    }

    return 0;
}
